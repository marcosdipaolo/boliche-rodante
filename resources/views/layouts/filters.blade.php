<!-- #filters -->
<input type="hidden" id="app_url" value="{{Config::get('app.url')}}">
<section id="filters">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-6 text-center">
				<label for="edad">Edad</label><br>
				<select id="edad" class="cats">
					<option value="">Sin selección</option>
					@foreach($edades as $edad)
					<option value="{{$edad->id}}">{{$edad->edad}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-6 text-center">
				<label for="genero">Género</label><br>
				<select id="genero" class="cats">
					<option value="">Sin selección</option>
					@foreach($generos as $gen)
					<option value="{{$gen->id}}">{{$gen->genero}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-6 text-center">
				<label for="info">Libro Informativo</label><br>
				<select id="info" class="cats">
					<option value="">Sin selección</option>
					@foreach($infos as $info)
					<option value="{{$info->id}}">{{$info->info}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-6 text-center d-flex justify-content-center">
				<div id="switches" style="max-width: 150px">
	            	<div>
		            	<span>Pop-up</span>&nbsp;&nbsp;&nbsp;
						<label class="switch">
							<input id="popup" name="popup" type="checkbox">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<!-- #pagination -->
	<div id="pagination" class="text-center">
	    
	</div>
	<!-- fin #pagination -->

	<!-- #filtered -->
	<div id="filtered">
	    
	</div>
	<!-- fin #filtered -->

</section>
@push('scripts')
<script type="text/javascript" src="{{asset('js/filters.js')}}"></script>
@endpush
<!-- end #filters -->
