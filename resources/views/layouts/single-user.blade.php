<!-- . single-user -->
<section class="pagina" id="single-user">
	<div class="container">
	    <h4>Perfil de usuario</h4><br>
	    <div class="row">
	    	<div class="col-md-6">
	    		<h5>Datos personales</h5><br>
			    <p>Nombre de usuario:&nbsp;&nbsp;<span style="color: #ccc;">{{$user->name}}</span>&nbsp;&nbsp;<span style="color:red">*</span></p>
			    <!-- Nombre -->
			    <p @if( ! $user->nombre) style="color: red; font-weight: bold;"@endif>Nombre:&nbsp;&nbsp;<span>{{$user->nombre}} <a href="/users/{{$user->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a> </span></p>
			    <!-- Apellido -->
			    <p @if( ! $user->apellido) style="color: red; font-weight: bold;"@endif>Apellido:&nbsp;&nbsp;<span>{{$user->apellido}} <a href="/users/{{$user->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a></span></p>
			    <!-- E-mail -->
			    <p @if( ! $user->email) style="color: red; font-weight: bold;"@endif>E-mail:&nbsp;&nbsp;<span style="color: #ccc;">{{$user->email}}</span>&nbsp;&nbsp;<span style="color:red">*</span></p>
			    <!-- Dirección -->
			    <p @if( ! $user->direccion) style="color: red; font-weight: bold;"@endif>Dirección:&nbsp;&nbsp;<span>{{$user->direccion}} <a href="/users/{{$user->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a>   </span></p>
			    <!-- Código Postal -->
			    <p @if( ! $user->cp) style="color: red; font-weight: bold;"@endif>C.P.:&nbsp;&nbsp;<span>{{$user->cp}} <a href="/users/{{$user->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a></span></p>
			    <!-- Ciudad -->
			    <p @if( ! $user->ciudad) style="color: red; font-weight: bold;"@endif>Ciudad:&nbsp;&nbsp;<span>{{$user->ciudad}} <a href="/users/{{$user->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a></span></p>
			    <!-- Provincia -->
			    <p @if( ! $user->provincia) style="color: red; font-weight: bold;"@endif>Provincia:&nbsp;&nbsp;<span>{{$provincias[$user->provincia] ?? ''}} <a href="/users/{{$user->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a></span></p>
			    <!-- País -->
			    <p @if( ! $user->pais) style="color: red; font-weight: bold;"@endif>País:&nbsp;&nbsp;<span>{{$user->pais}}</span></p>
			    <!-- Teléfono -->
			    <p @if( ! $user->telefono) style="color: red; font-weight: bold;"@endif>Teléfono:&nbsp;&nbsp;<span>{{$user->telefono}} <a href="/users/{{$user->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a></span></p>
			    <!-- Botón Checkout -->
			    @if( ! $user->hasNullFields() && Session::get('cart'))<br>
			    <a href="/checkout"><button class="btn black">Checkout</button></a>
			    @endif
			    <br>
			    <br>
			    <span style="color:red">*</span>&nbsp;&nbsp;<small>Si deseás cambiar tu nombre de usuario o email ponete en contacto con nosotros</small>
			</div>
			<div class="col-md-6">
				<h5>Historial de compras</h5><br>
				<div id="accordion">
					@foreach(auth()->user()->checkouts as $checkout)
					@php
						$productos = unserialize($checkout->productos);
					@endphp
					
					<div class="card">
						<div class="card-header" id="heading_{{$loop->iteration}}">
						    <p class="compra" data-toggle="collapse" data-target="#collapse_{{$loop->iteration}}" aria-expanded="true" aria-controls="collapse_{{$loop->iteration}}">
						      {{ $checkout->created_at->day }}-{{ $checkout->created_at->month }}-{{ $checkout->created_at->year }}
						    </p>
						</div>

						<div id="collapse_{{$loop->iteration}}" class="collapse" aria-labelledby="heading_{{$loop->iteration}}" data-parent="#accordion">
						  <div class="card-body">
						    <table class="table table-sm">
						    	<thead>
							    <tr>
							      <th scope="col">Libro</th>
							      <th scope="col">Cantidad</th>
							      <th scope="col">Unit.</th>
							      <th scope="col">Subtotal</th>
							    </tr>
							  </thead>
							  <tbody>
							  	
							  
						    	@php
						    		$total = 0;
						    	@endphp
						    
						    @foreach($productos as $libroId => $libroData)

								@php
						    		$libro = App\Libro::find($libroId);
						    		$cantidad = $libroData['cantidad'];									
						    		$precio_unitario = $libroData['precio_unitario'];		
						    		$total += $precio_unitario * $cantidad;
								@endphp
								<tr>
							      <td>{{$libro->titulo}}</td>
							      <td>{{$cantidad}}</td>
							      <td>${{$precio_unitario}}</td>
							      <td>${{$precio_unitario * $cantidad}}</td>
							    </tr>
								
						    @endforeach
								</tbody>
								<tfoot>
									<td></td>
									<td></td>
									<th>Total</th>
									<th>${{$total}}</th>
								</tfoot>
							</table>
						  </div>
						</div>
						</div>
						@endforeach
				</div><!-- / div.accordion -->
				<small>* click en las filas para expandir</small>
			</div>
	    </div>
	</div>
</section>
<!-- fin . single-user -->
