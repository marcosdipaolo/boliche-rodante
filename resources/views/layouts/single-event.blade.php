<!-- . single-event -->
<section id="single-event" class="pagina">
	<div class="container-fluid img" style="background-image: url('{{Storage::url($evento->img)}}')"></div>
	<br>
	<br>
	<div class="container">
		<h4 class="text-center">{{$evento->titulo}}</h4>
		<br><br>
		{!!$evento->texto!!}
	</div>
    
</section>
<!-- fin . single-event -->