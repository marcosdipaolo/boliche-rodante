window.onload = function () {
    const edad = document.querySelector('#edad');
    const genero = document.querySelector('#genero');
    const info = document.querySelector('#info');
    const popup = document.querySelector('#popup');
    const selects = Array.from(document.querySelectorAll('select.cats'));
    const app_url = document.querySelector('#app_url').value;
    const divFiltered = document.querySelector('div#filtered');
    const divPagination = document.querySelector('div#pagination');
    const divLibros = document.querySelector('div#libros');
    const csrf_token = document.querySelector('meta[name=csrf-token]').content;
    const pageLinks = document.querySelector('.page-link');

    let prev;
    let next;
    let links;
    let current;
    let last_page;

    const eventsChange = target => {
        target.addEventListener('change', e => {
            query(edad.value, genero.value, info.value, popup.checked);
        });
    }
    selects.forEach(select => {
        eventsChange(select);
    });
    eventsChange(popup);
    const eventsPagination = () => {
        links = Array.from(document.querySelectorAll('.page-link.filters'));
        next = links.pop();
        prev = links.shift();
        links.forEach(link => {
            link.addEventListener('click', function (e) {
                e.preventDefault();
                query(edad.value, genero.value, info.value, popup.checked, parseInt(this.dataset.page));
            });
        });
        next.addEventListener('click', e => {
            e.preventDefault();
            current = current >= last_page ? current : current + 1;
            query(edad.value, genero.value, info.value, popup.checked, current);
        });
        prev.addEventListener('click', e => {
            e.preventDefault();
            current = current <= 1 ? 1 : current - 1;
            query(edad.value, genero.value, info.value, popup.checked, current);
        });

    }

    const query = (edad, genero, info, popup, page = 1) => {
        current = page;
        const edadString = edad ? 'edad=' + edad + '&' : '';
        const generoString = genero ? 'genero=' + genero + '&' : '';
        const infoString = info ? 'info=' + info + '&' : '';
        const popupString = popup ? 'popup=true&' : '';
        const pageString = page ? 'page=' + page : '';
        let queryString = app_url + '/api/filter?' + edadString + generoString + infoString + popupString + pageString;
        fetch(queryString)
            .then(response => {
                console.log(response);
                return response.json();
            })
            .then(response => {
                console.log(response);
                last_page = response.last_page;
                let libros = `
			<br>
			<br>
			<br>
			<div class="container">
			<div class="row">
			`;
                let index = 0;
                if (response.data.length) {
                    response.data.forEach(libro => {
                        index++;
                        libros += `
					<div class="col-lg-2 col-md-4 col-sm-6 mt-4">
					<div class="item">
					<div class="item-img">
					<a href="/libros/${libro.slug}"><img src="/storage/${libro.img}" alt="img"></a>
					</div>
					<div class="item-info">
					<h3><a href="/libros/${libro.slug}">${libro.titulo}</a></h3>
					<h6><span class="price">$${libro.precio}.-</span> / <a href="#" onclick="event.preventDefault(); document.querySelector('form#comprarAhora${index}').submit()" style="border: none">Comprar ahora</a></h6>
					<form action="/carrito" method="POST" id="comprarAhora${index}">
					<input type="hidden" name="_token" value="${csrf_token}">
					<input type="hidden" name="id" value="${libro.id}">
					</form>
					</div>
					</div>
					</div>
					`;
                    });


                } else {
                    libros += `
				<h4 class="text-center w-100">No se encontró ningún título con ese criterio</h4>
				`;
                }
                libros += `
			</div>
			</div>
			`;
                divLibros.innerHTML = '';
                divFiltered.innerHTML = libros;
                const paginate = () => {
                    let paginationString = `
				<nav aria-label="Page navigation example">
					<ul class="pagination">`; // cabeza de paginación

                    paginationString += `
						<li class="page-item${current <= 1 ? ' disabled' : ''}">
							<a class="page-link filters" href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>`; // &laquo;
                    for (let i = 1; i <= response.last_page; i++) {
                        paginationString += `<li class="page-item${current == i ? ' active' : ''}"><a class="page-link filters" data-page="${i}" href="#">${i}</a></li>`; // páginas
                    }

                    paginationString += `
						<li class="page-item${current >= last_page ? ' disabled' : ''}">
							<a class="page-link filters" href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>`; //&raquo;

                    paginationString += `
						</ul>
					</nav>`; // cierre de paginación
                    divPagination.innerHTML = paginationString;
                    eventsPagination();
                }
                if (response.last_page > 1) {
                    paginate();
                } else {
                    divPagination.innerHTML = '';
                }
                ;


            })
            .catch(err => console.log(err));
    } // fn query
} // windows onload
