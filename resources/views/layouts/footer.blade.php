<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="address">
                    <h4>Nuestra dirección</h4>
                    <h6>{{config('boliche.direccion')}} o whatsapp {{config('boliche.whatsapp')}}</h6>
                    <h6>Email: {{config('app.email')}}</h6>
                </div>
                <div class="timing">
                    <h4>Atención</h4>
                    <h6>Lu - Vie: 10 am – 19 pm</h6>
                    <h6>​​Sábado: 10 am – 15 pm</h6>
                </div>
            </div>
            <div class="col-md-3">
                <div class="navigation">
                    <h4>Navegación</h4>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="/about">Nosotros</a></li>
                        <li><a href="/terminos">Términos y condiciones</a></li>
                        <li><a href="/shop">Shop</a></li>
                    </ul>
                </div>
                <div class="navigation">
                    <h4>Ayuda</h4>
                    <ul>
                        {{-- <li><a href="/envios-y-devoluciones">Envíos y devoluciones</a></li> --}}
                        <li><a href="/privacidad">Privacidad</a></li>
                        {{-- <li><a href="/faq">FAQ’s</a></li> --}}
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form">
                    <h3>Contacto rápido</h3>
                    <h6>Te contestaremos tan rápido nos sea posible.</h6>
                    <form method="POST" action="{{route(Routes\GeneralRoutes::CONTACTO)}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <input placeholder="Nombre" name="nombre" required>
                            </div>
                            <div class="col-md-6">
                                <input type="email" name="email" placeholder="Email" required>
                            </div>
                            <div class="col-md-12">
                                <textarea name="mensaje" placeholder="Mensaje"></textarea>
                            </div>
                            <div class="col-md-12">
                                <button class="btn black">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h5>(C) 2018. Diseño y Desarrollo Marcos Di Paolo</h5>
                </div>
                <div class="col-md-6">
                    <div class="share align-middle">
                        <a href="https://www.facebook.com/profile.php?id=100008084536272" target="_blank"><span class="fb"><i class="fab fa-facebook"></i></span></a>
                        <a href="https://instagram.com/bolicherodante" target="_blank"><span class="instagram"><i class="fab fa-instagram"></i></span></a>
                  {{--       <a href=""><span class="twitter"><i class="fab fa-twitter"></i></span></a>
                        <a href=""><span class="pinterest"><i class="fab fa-pinterest"></i></span></a>
                        <a href=""><span class="google"><i class="fab fa-google-plus"></i></span></a> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- JQUERY -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
{{-- BOOTSTRAP --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
@stack('scripts')
<script src="/js/post-thumbnail.js"></script>
<script type="text/javascript" src="/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.10/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="/js/smoothScroll.js"></script>
<script type="text/javascript" src="/js/scrollReveal.js"></script>
@isset($quill)
<script src="/js/quill.js"></script>

<script>
    var toolbarOptions = [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],             // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],
      ['link', 'image', 'video'],
      ['clean']                                         // remove formatting button
    ];
    var quill = new Quill('#textarea', {
        modules: {
            toolbar: toolbarOptions
        },
        theme: 'snow',
        placeholder: 'Escribí tu artículo...'
      });
</script>
@endisset
<script src="/js/custom.js"></script>
<script src="/js/app.js"></script>
<script src="/js/precios.js"></script>
<script src="/js/compras.js"></script>
</body>
</html>
