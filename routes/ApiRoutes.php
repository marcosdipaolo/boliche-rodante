<?php

namespace Routes;

use App\Http\Controllers\API\LibroController;
use App\Http\Controllers\CacheController;
use Illuminate\Contracts\Routing\Registrar;
use LaravelBA\RouteBinder\Routes;

class ApiRoutes implements Routes
{
    public function addRoutes(Registrar $router)
    {
        $router->group(['prefix' => 'api', 'middleware' => 'api'], function() use ($router){
            $router->get('/filter', LibroController::class . '@filter');

            $router->group(['prefix' => 'commands', 'middleware' => 'commands'], function() use ($router){
                //route cache:
                $router->post('/route-cache', ['uses' => CacheController::class . '@routesCache']);
                $router->post('/view-cache', ['uses' => CacheController::class . '@viewCache']);

                //clear route cache:
                $router->post('/route-clear', ['uses' => CacheController::class . '@routesClear']);

                // dumps autoload
                $router->post('/dump-autoload', ['uses' => CacheController::class . '@dumpAutoload']);

                //Clear config cache:
                $router->post('/config-cache', ['uses' => CacheController::class . '@configCache']);

                // Clear application cache:
                $router->post('/clear-cache', ['uses' => CacheController::class . '@clearCache']);

                // Clear view cache:
                $router->post('/view-clear', ['uses' => CacheController::class . '@clearView']);

                $router->post('/migrate', ['uses' => CacheController::class . '@migrate']);
            });
        });
    }
}
