<?php

namespace App;
use App\Traits\Slug;
use App\Traits\Imagen;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Laravel\Scout\Searchable;

/**
 * App\Libro
 *
 * @property int $id
 * @property string $isbn
 * @property string $titulo
 * @property string $slug
 * @property string $descripcion
 * @property string $editorial
 * @property string $autor
 * @property string|null $coleccion
 * @property int|null $edad
 * @property string|null $distribuidor
 * @property int|null $precio
 * @property int|null $cantidad
 * @property int|null $anio
 * @property int $destacados2
 * @property int $destacados1
 * @property int $popup
 * @property string $img
 * @property int $activado
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Edad[] $edades
 * @property-read int|null $edades_count
 * @property-read Collection|Genero[] $generos
 * @property-read int|null $generos_count
 * @property-read Collection|Info[] $infos
 * @property-read int|null $infos_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereActivado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereAnio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereAutor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereColeccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereDestacados1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereDestacados2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereDistribuidor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereEdad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereEditorial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereIsbn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro wherePopup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereTitulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libro whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Libro extends Model
{
    use Slug;

    use Imagen;

    use Searchable;

    public function getRouteKeyName()
    {
    	return 'slug';
    }

    /**
     * @return BelongsToMany
     */
    public function edades()
    {
    	return $this->belongsToMany(Edad::class);
    }

    /**
     * @return BelongsToMany
     */
    public function generos()
    {
    	return $this->belongsToMany(Genero::class);
    }

    /**
     * @return BelongsToMany
     */
    public function infos()
    {
    	return $this->belongsToMany(Info::class);
    }
}
