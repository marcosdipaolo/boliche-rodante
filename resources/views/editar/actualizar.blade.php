<section id="actualizar">
	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-md-6 px-5">
	            <h4 class="text-center">Actualización de Precios y Stock</h4><br>
	            @include('layouts.errors')
	            <form action="/actualizar-precios" method="POST" enctype="multipart/form-data">
	            	@csrf
	            	<div class="form-group">
	            		<label for="editorial">Editorial</label>
	            		<input type="text" name="editorial" id="editorial" class="form-control">
	            	</div><br>
	            	<div class="form-group">
	            		<label for="xls">Archivo .xls</label>
	            		<input type="file" name="xls" id="xls">
	            	</div>
	            	<br>
	            	<div class="form-group"><button class="btn">actualizar</button></div>
	            </form>
	        </div>
	    </div>
	</div>    
</section>
