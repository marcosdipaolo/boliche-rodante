<?php

namespace App\Http\Middleware;

use App\Libro;
use App\Post;
use Closure;
use Illuminate\Support\Facades\View;

class ViewMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	/**
        * Array de PROVINCIAS para a edición y perfil de usuario
        */

    	View::composer(['layouts.edit-user', 'layouts.single-user', 'layouts.checkout'], function($view){
    		$view->with('provincias', [
    			'C' => 'CABA',
    			'B' => 'Buenos Aires',
    			'K' => 'Catamarca',
    			'H' => 'Chaco',
    			'U' => 'Chubut',
    			'X' => 'Córdoba',
    			'W' => 'Corrientes',
    			'E' => 'Entre Ríos',
    			'P' => 'Formosa',
    			'Y' => 'Jujuy',
    			'L' => 'La Pampa',
    			'F' => 'La Rioja',
    			'M' => 'Mendoza',
    			'N' => 'Misiones',
    			'Q' => 'Neuquén',
    			'R' => 'Río Negro',
    			'S' => 'Salta',
    			'J' => 'San Juan',
    			'D' => 'San Luis',
    			'Z' => 'Santa Cruz',
    			'S' => 'Santa Fe',
    			'G' => 'Santiago del Estero',
    			'V' => 'Tierra del fuego',
    			'T' => 'Tucumán'
    		]);
    	});
    	View::composer(['layouts.recomendados', 'layouts.sidebar'], function($view){
    		$view->with('recomendados', Libro::where([['destacados1', 1], ['activado', 1], ['img', '!=', '/images/default.jpg']])->take(4)->get());
    	});
    	View::composer(['layouts.muy-recom', 'layouts.sidebar'], function($view){
    		$view->with('recientes', Libro::where([['destacados2', 1], ['activado', 1], ['img', '!=', '/images/default.jpg']])->take(5)->get());
    	});
    	View::composer(['layouts.all-books'], function($view){
    		$view->with('libros', Libro::orderBy('titulo', 'ASC')->where([['activado', 1], ['img', '<>', '']])->paginate(18));
    	});
    	View::composer(['layouts.eventos', 'layouts.sidebar', 'home', 'layouts.header-inc.main-menu'], function($view){
    		$view->with('sidebar_posts', Post::take(4)->get());
    	});
    	return $next($request);
    }
}
