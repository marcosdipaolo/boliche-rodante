$(document).ready(function() {

    $('section#precios input.precio').on('blur', function(e) {
       
        var libro = $(this).data('libro');
        var $exito = $(this).parent().siblings('.alert-success');
        var $error = $(this).parent().siblings('.alert-danger');
        var data = {
            _token: $(this).siblings('input[name=_token]').val(),
            _method: $(this).siblings('input[name=_method]').val(),
            precio: $(this).val(),
            libro: $(this).data('libro'),
        }

        $.ajax({
            type: 'POST',
            url: `/precios/${libro}`,
            data: data,
            success: libro => {
                $exito.fadeIn(200);
                setTimeout(function() { $exito.fadeOut(200); }, 1000);
            },
            error: function() {
                $error.fadeIn(200);
                setTimeout(function() { $error.fadeOut(200); }, 1000);
            }

        });

    });

    

}); // final
