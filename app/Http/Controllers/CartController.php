<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Libro;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CartController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('carrito');
    }

    /**
     * Agrega un item al carrito
     *
     * @param Request $request
     * @return view('carrito')
     */
    public function store(Request $request)
    {
        $libro = Libro::find($request->id);
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($libro);
        $request->session()->put('cart', $cart);
        // return dd(session('cart'));
        return view('carrito');
    }

    /**
     * @param Request $request
     * @param $libro_id
     * @return RedirectResponse
     */
    public function update(Request $request, $libro_id)
    {
        $libro = Libro::find($libro_id);
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        if ($oldCart) {
            $cart->remove($libro);
        }
        $request->session()->put('cart', $cart);
        return back();
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        session(auth()->user()->id)->clear();
    }
}
