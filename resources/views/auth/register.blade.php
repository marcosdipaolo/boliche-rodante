@extends('layouts.master')

@section('content')

<section class="static">
        <div class="container">
            <h1>Mi Cuenta / Registro</h1>
            @include('layouts.errors')
            <div class="form">
                <form method="POST" action="{{ route('register') }}">
                        @csrf

                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" autofocus type="text" placeholder="nombre de usuario" required>
                                     @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                    <span class="required-star">*</span>
                                </div>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                                     @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    <span class="required-star">*</span>
                                </div>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required>
                                    <span class="required-star">*</span>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" name="password_confirmation" required placeholder="Repetir Contraseña">
                                    <span class="required-star">*</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <button class="btn black">Register</button>
                            <h5>Ya tenés una cuenta? <a href="/login">Logueate acá</a></h5>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
