<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class Contacto extends Mailable
{
    use Queueable, SerializesModels;

    protected $user; 
    protected $mensaje; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $mensaje)
    {
       $this->user = $user;
       $this->mensaje = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->user->email)->markdown('emails.contacto')
        ->with(['user' => $this->user, 'mensaje' => $this->mensaje]);
    }
}