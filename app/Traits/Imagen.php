<?php 
namespace App\Traits;
use App\Libro;
use App\Post;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait Imagen 
{
	public function subirImagen(UploadedFile $imagen)
	{
		/**
		 * PARA LOS LIBROS
		 */
		if($this instanceof Libro) {
			/*
			 * borramos la imagen existente
			 */
			if($this->img) {
				\Storage::disk('public')->delete($this->img);
			}
			/*
			 * subimos la imagen
			 */
			$ext = $imagen->extension();
			$filename = $this->isbn . '_' . time() . '.' . $ext;
			$tapa = \Image::make($imagen)->resize(null, 560, function ($constraint) {
			    $constraint->aspectRatio();
			});
			$tapa_path = storage_path('app/public/img/tapas/') . $filename;
			$tapa->save($tapa_path);
			return 'img/tapas/' . $filename;
		}

		/**
		 * PARA LOS POSTS
		 */

		elseif( $this instanceof Post ){
			/*
			 * borramos la imagen existente
			 */
			if($this->img) {
				\Storage::disk('public')->delete($this->img);
			}
			/*
			 * subimos la imagen
			 */
			$ext = $imagen->extension();
			$filename = $this->slug . '_' . time() . '.' . $ext;
			$tapa = \Image::make($imagen)->fit(1110, 738, function ($constraint) {
			    $constraint->upsize();
			});
			$tapa_path = storage_path('app/public/img/eventos/') . $filename;
			$tapa->save($tapa_path);
			return 'img/eventos/' . $filename;
		}
	}



	public function borrarImg() 
	{
		if ($this->img && $this->img != '/images/tapas/default.jpg') {
			\Storage::disk('public')->delete($this->img);
			return true;
		} else {
			return false;
		}
		
	}

}