<?php

namespace App;

class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    /**
     * Cart constructor.
     * @param $oldCart
     */
    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    /**
     * @param Libro $libro
     */
    public function add(Libro $libro)
    {
        $id = $libro->id;
        $item = ['cant' => 0, 'precio' => $libro->precio, 'item' => $libro];
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $item = $this->items[$id];
            }
        }
        $item['cant']++;
        $item['precio'] = $libro->precio * $item['cant'];
        $this->items[$id] = $item;
        $this->totalQty++;
        $this->totalPrice += $libro->precio;
    }

    /**
     * @param Libro $libro
     */
    public function remove(Libro $libro)
    {
        $id = $libro->id;
        if (array_key_exists($id, $this->items)) {
            if ($this->items[$id]['cant'] > 1) {
                $this->items[$id]['cant']--;
                $this->items[$id]['precio'] = $libro->precio * $this->items[$id]['cant'];
            } else {
                unset($this->items[$id]);
            }
        }
        $this->totalQty--;
        $this->totalPrice -= $libro->precio;
    }
}
