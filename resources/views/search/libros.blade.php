@extends('layouts.master')
@section('content')
<span id="body_class" data-class="search" class="d-none"></span>
<section class="all-books pagina" id="search">
    <a href="/shop" @if(! $_GET) style="display: none;" @endif><p>Volver</p></a>
    <div class="container">
    <div class="title">
        <h2>Búsqueda: {{ $cadena }}</h2>
        <hr>
        <br>
        <br>
        <br>
        <br>
          {{ $librosPaged->links()}}
    </div>

    	@if(count($librosPaged))
		    <div class="row">
		        @foreach($librosPaged as $libro)
		            <div class="col-lg-2 col-md-3 col-sm-4">
		                <div class="item">
		                    <div class="item-img">
		                        <img src="{{resolveImage($libro->img)}}" alt="img">
		                    </div>
		                    @include('layouts.botones-editar-borrar')
		                    <div class="item-info">
                                @if($libro->cantidad <= 0)
                                    <div class="position-relative">
                                        <span class="badge badge-danger">No hay stock</span>
                                    </div>
                                @endif
		                        <h3><a href="/libros/{{$libro->slug}}">{{$libro->titulo}}</a></h3>
		                        <h6><span class="price">${{$libro->precio}}.-</span>
                                @if(!$libro->cantidad <= 0)
                                    / <a href="#" onclick="document.getElementById('add-to-cart-{{$libro->id}}').submit()">Comprar ahora</a></h6>
                                    <form id="add-to-cart-{{$libro->id}}" action="/carrito" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$libro->id}}">
                                    </form>
                                @endif
		                    </div>
		                </div>
		            </div>
		        @endforeach
		    </div>
        @else
        <p class="text-center">No se encontraron resultados.</p>
        @endif

     {{$librosPaged->links()}}
</div>
</section>
@endsection
