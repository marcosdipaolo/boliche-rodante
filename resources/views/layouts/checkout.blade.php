<section class="pagina" id="checkout">
    <div class="container">
        @if(session()->has('checkoutSuccess'))
            <div class="alert alert-success">{{session()->get('checkoutSuccess')}}</div>
        @elseif(session()->has('checkoutError'))
            <div class="alert alert-danger">{{session()->get('checkoutError')}}</div>
        @endif
        <div class="row">
            <div class="col-lg-8 col-md-7 cart-table mb-5">
                <h4>Tus datos</h4><br>
                <div class="row">
                    <div class="col-lg-6">
                        <p>Nombre de usuario:&nbsp;&nbsp;<span>{{auth()->user()->name}}</span></p>
                        <p @if( ! auth()->user()->nombre) style="color: red; font-weight: bold;"@endif>Nombre:&nbsp;&nbsp;<span>{{auth()->user()->nombre}} <a href="/users/{{auth()->user()->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a> </span></p>
                        <p @if( ! auth()->user()->apellido) style="color: red; font-weight: bold;"@endif>Apellido:&nbsp;&nbsp;<span>{{auth()->user()->apellido}} <a href="/users/{{auth()->user()->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a>   </span></p>
                        <p>E-mail:&nbsp;&nbsp;<span>{{auth()->user()->email}}</span></p>
                        <p @if( ! auth()->user()->direccion) style="color: red; font-weight: bold;"@endif>Dirección:&nbsp;&nbsp;<span>{{auth()->user()->direccion}} <a href="/users/{{auth()->user()->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a>   </span></p>
                    </div>
                    <div class="col-lg-6">
                        <p @if( ! auth()->user()->cp) style="color: red; font-weight: bold;"@endif>C.P.:&nbsp;&nbsp;<span>{{auth()->user()->cp}} <a href="/users/{{auth()->user()->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a></span></p>
                        <p @if( ! auth()->user()->ciudad) style="color: red; font-weight: bold;"@endif>Ciudad:&nbsp;&nbsp;<span>{{auth()->user()->ciudad}} <a href="/users/{{auth()->user()->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a></span></p>
                        <p  @if( ! auth()->user()->provincia) style="color: red; font-weight: bold;"@endif>Provincia:&nbsp;&nbsp;<span>{{$provincias[auth()->user()->provincia] ?? ''}} <a href="/users/{{auth()->user()->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a>   </span></p>
                        <p @if( ! auth()->user()->pais) style="color: red; font-weight: bold;"@endif>País:&nbsp;&nbsp;<span>{{auth()->user()->pais}} <a href="/users/{{auth()->user()->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a>   </span></p>
                        <p @if( ! auth()->user()->telefono) style="color: red; font-weight: bold;"@endif>Teléfono:&nbsp;&nbsp;<span>{{auth()->user()->telefono}} <a href="/users/{{auth()->user()->id}}/edit">&nbsp;&nbsp;<i class="far fa-edit"></i></a>   </span></p>
                    </div>
                    @if(auth()->user()->hasNullFields())<small style="color:red">* Para poder efectuar la orden tenés que contar con todos los datos personales cargados</small>@endif

                </div><br><br><br>
                @if( ! auth()->user()->hasNullFields())
                    <h5>Items agregados al carrito de compras.</h5>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tapa</th>
                            <th scope="col">Título</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Precio</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($cart_items as $item)
                            @php
                                $libro = $item['item'];
                            @endphp
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td><img src="{{resolveImage($libro->img)}}" width="50"></td>
                                <td><a href="/libros/{{$libro->slug}}">{{$libro->titulo}}</a><br><small>{{$libro->autor}}, {{$libro->editorial}}</small></td>
                                <td align="center">{{$item['cant']}}
                                    @if($item['cant'] > 0)
                                        <br>
                                        <br>
                                        <form action="/carrito/{{$libro->id}}" method="POST">
                                            @method('PUT')
                                            @csrf
                                            <button class="no-style"><i class="far fa-trash-alt"></i></button>
                                        </form>
                                    @endif
                                </td>
                                <td>${{$item['precio']}}.-</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col">TOTAL</th>
                        <th scope="col"></th>
                        {{-- <th scope="col">${{Cart::getSubTotal()}}</th> --}}
                        <th scope="col" class="text-danger">${{Session::get('cart')->totalPrice ?? 0}}.-</th>
                        </tfoot>
                    </table><br>
                    <h3 style="color:red">* IMPORTANTE</h3><br>
                    <p>Al finalizar el pago es importante darle click al botón para volver al sitio y finalizar el proceso.</p>
                    <p>En todos los casos consultar previamente costos y modalidad de envío, o bien dirección de retiro. Ante cualquier duda comunicarse con el +54 9 11 67634382.</p>
                    <br>
                    <a href="/shop"><button class="btn orange">SEGUIR comprando</button></a>
                    @if(Session::get('cart')->totalQty >= 1)
                        <style>
                            button.mercadopago-button {
                                border-radius: 2px;
                                background: #252525;
                                border: 2px solid #252525;
                                opacity: 1;
                                color: #fff;
                                text-transform: uppercase;
                                padding: 5px 20px ;
                                font-size: 14px;
                                font-weight: 500;
                                letter-spacing: .4px;
                            }
                        </style>
                        <form action="/mercadopago" method="POST" style="float: right"  data-header-color="#C2452F" data-elements-color="#C2452F">
                            @csrf
                            <script
                                src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
                                data-public-key= "{{config('mercadopago.pubilc_key')}}"
                                data-preference-id="{{$preference->id}}" data-elements-color="#C2452F">
                            </script>
                        </form>
                    @endif
                @endif
            </div>
            <!-- SIDEBAR -->
            <div id="aside-cont" class="col-md-5 col-lg-4">
                @include('layouts.sidebar')
            </div>
            <!-- FIN SIDEBAR -->
        </div>


    </div>
</section>
<!-- fin . carrito -->
