@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <section class="static about-sec">
            <div class="container">
                <h1>Mi cuenta / Login</h1>
                @include('layouts.errors')
                <div class="form">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <input class="{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" placeholder="E-mail" name="email" value="{{ old('email') }}" required autofocus>
                                <span class="required-star">*</span>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input type="password" placeholder="Contraseña" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                <span class="required-star">*</span>
                                 @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <button class="btn black">Login</button><br><br><br>
                                <h5>no estás registrado? <a href="/register">Registrate acá</a></h5>
                                <h5>{{ __('olvidaste tu contraseña?') }}<a href="{{ route('password.request') }}">
                                    {{__('Recuperala acá')}}
                                </a></h5>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
