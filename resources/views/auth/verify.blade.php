@extends('layouts.master')

@section('content')
<!-- . verify-email -->
<section class="verify-email">
    <div id="reset-password" class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Verificá tu e-mail') }}</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('Un nuevo link fue enviado a tu correo electrónico.') }}
                            </div>
                        @endif

                        {{ __('Antes de continuar por favor verificá tu dirección de correo electrónico.') }}
                        {{ __('Si no recibiste el email') }}, <a href="{{ route('verification.resend') }}">{{ __('hacé click acá para solicitarlo nuevamente.') }}</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- fin . verify-email -->
@endsection
