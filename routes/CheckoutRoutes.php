<?php

namespace Routes;

use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\MercadopagoController;
use Illuminate\Contracts\Routing\Registrar;
use LaravelBA\RouteBinder\Routes;

class CheckoutRoutes implements Routes
{
    public function addRoutes(Registrar $router)
    {
        $router->group(['middleware' => 'web'], function() use ($router){
            $router->get('/compras', CheckoutController::class . '@adminIndex');
            $router->put('/compras/{compra}', CheckoutController::class . '@sent');
            $router->get('/checkout/create', CheckoutController::class . '@create');
            $router->get('/checkout/ok', CheckoutController::class . '@authAnswer');
            $router->get('/checkout/error', CheckoutController::class . '@errorResponse');
            $router->get('/checkout', CheckoutController::class . '@index');
            $router->get('/procesar-pago', CheckoutController::class . '@index');
            $router->post('/mercadopago', ['uses' => MercadopagoController::class . '@mercadopago']);
        });
    }
}
