@extends('layouts.master')

@section('content')
<span id="body_class" data-class="home" class="d-none"></span>
    @include('layouts.errors')
	@if(session()->has('success'))
	<div class="alert alert-success container">{{session()->get('success')}}</div>
    @elseif(session()->has('error'))
    <div class="alert alert-danger container">{{session()->get('error')}}</div>
	@endif
    @include('layouts.slider')

    @include('layouts.recomendados')

    @include('layouts.about')

    @include('layouts.muy-recom')

{{--    @include('layouts.tres-widgets')--}}

{{--    @include('layouts.ofertas')--}}



@endsection
