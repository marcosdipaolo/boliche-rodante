<?php

namespace App\Http\Controllers;

use App\Post;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin'])->except('index', 'show');
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $eventos = Post::orderBy('created_at', 'DESC')->paginate(1);
        return view('eventos', compact('eventos'));
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        return view('editar.crear-evento');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'bail|required|string|min:2',
            'texto' => 'required|string|min:10',
            'img' => 'image|required|max:1499'
        ]);
        $evento = new Post;
        $evento->titulo = $request->titulo;
        $evento->user_id = $request->user_id;
        $evento->slug = $evento->slug();

        $evento->img = $request->hasFile('img') ? $evento->subirImagen($request->img) : null;
        $evento->texto = $request->texto;
        $evento->save();
        session()->flash('exito', 'El mensaje se creó con éxito');
        return view('single-event', compact('evento'));
    }

    /**
     * @param Post $evento
     * @return Factory|View
     */
    public function show(Post $evento)
    {
        return view('single-event', compact('evento'));
    }

    /**
     * @param Post $evento
     * @return Factory|View
     */
    public function edit(Post $evento)
    {
        return view('editar.editar-evento', compact('evento'));
    }

    /**
     * @param Request $request
     * @param Post $evento
     * @return Factory|View
     */
    public function update(Request $request, Post $evento)
    {
        $request->validate([
            'titulo' => 'required|string|min:2',
            'texto' => 'string|min:10',
            'img' => 'image|max:1499'
        ]);

        $evento->titulo = $request->titulo;
        $evento->slug = $evento->slug();

        $evento->img = $request->hasFile('img') ? $evento->subirImagen($request->img) : $evento->img ?? null;
        $evento->texto = $request->texto;
        $evento->save();
        session()->flash('exito', 'El mensaje se editó con éxito');
        return view('editar.eventos');
    }

    /**
     * @param Post $evento
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(Post $evento)
    {
        $evento->borrarImg();
        $evento->delete();
        session()->flash('exito', 'El mensaje se borró con éxito');

        return redirect('/admin/eventos');
    }
}
