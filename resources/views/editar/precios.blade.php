<!-- . precios -->

<section id="precios">
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
            </div>
        @endif  
        @if (session('not_found'))
            <div class="alert alert-danger" role="alert">
                {{ session('not_found') }}
            </div>
        @endif    
    </div>    
    
    @include('editar.botones')
    <br>
    <div class="container">
        <div class="row justify-content-center">
    		<div class="col-lg-12">
    			{{ $libros->links() }}
                <br>
                <a href="/actualizar-precios"><button class="btn">Actualizar precios con un archivo .XLS</button></a>
                <br>
                <br>
    			<table class="table table-sm">
    				<thead>
    					<tr>
                            <th>#</th>
    						<th>Título del libro</th>
                            <th>ISBN</th>
    						<th>Editorial</th>
    						<th>Precio</th>
    					</tr>
    				</thead>
    				<tbody>
    					@foreach($libros as $libro)
    					<tr>
                            <td>{{isset($_GET['page']) ? $loop->iteration + (($_GET['page'] - 1 ) * 100) : $loop->iteration}}</td>
    						<td>{{$libro->titulo}}</td>
                            <td>{{$libro->isbn}}</td>
    						<td>{{$libro->editorial}}</td>
    						<td><form method="post" action="/precios/{{$libro->slug}}">
    							@csrf 
    							@method('PUT')
    							<input data-libro="{{$libro->slug}}" class="precio" type="number" name="precio" value="{{$libro->precio}}"></form>
                                <div class="alert alert-success">Precio editado.</div>
                                <div class="alert alert-danger">Ocurrió un error.</div>
    						</td>
    					</tr>
    					@endforeach
    				</tbody>
    			</table>
    			{{ $libros->links() }}
    		</div>
    	</div>
    </div>
</section>
<!-- fin . precios -->