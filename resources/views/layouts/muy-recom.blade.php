@if(count($recientes))
<!--  Muy Recomendados / Recent  -->
<section class="recent-book-sec parcial" @if($_GET) style="display: none; margin-bottom: 0;" @endif>
    <div class="container">
        <div class="title" @if($_GET) style="display: none;" @endif>
            <h2>Últimas novedades</h2>
            <hr>
        </div>
        <div class="row justify-content-center" @if($_GET) style="display: none;" @endif>
            @foreach($recientes as $reciente)
                <div class="col-sm-4 col-md-3 col-lg-2 mt-4">
                    <div class="item">
                        <div class="item-img">
                            <a href="/libros/{{$reciente->slug}}"><img src="@if($reciente->img){{Storage::url($reciente->img)}}@else {{asset('/images/default.jpg')}} @endif" alt="img"></a>
                        </div>
                        <div class="item-info">
                            @if(!$reciente->cantidad > 0)
                                <div class="position-relative">
                                    <span class="badge badge-danger">No hay stock</span>
                                </div>
                            @endif
                            <a href="/libros/{{$reciente->slug}}"><h3>{{$reciente->titulo}}</h3></a>
                            <h6><span class="price">${{$reciente->precio}}.-</span>
                            @if($reciente->cantidad > 0)
                            / <a href="#" onclick="event.preventDefault(); document.querySelector('form#add-to-cart-{{$reciente->id}}').submit()" style="border: none">Comprar ahora</a></h6>
                            <form action="/carrito" method="POST" id="add-to-cart-{{$reciente->id}}">
                                 @csrf
                                <input type="hidden" name="id" value="{{$reciente->id}}">
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endif
