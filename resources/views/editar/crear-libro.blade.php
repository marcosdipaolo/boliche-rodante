@extends('editar.app')

@section('content')
<section id="crear-libro">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-6">
				<h4 class="text-center">Creamos un libro</h4><br>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 px-5">
				{!! Form::open(['action' => [App\Http\Controllers\LibroController::class . '@store'], 'files' => true]) !!}
				<!-- . titulo form-group -->
				<div class="titulo form-group">
					{{ Form::label('titulo', 'Título') }}
					{{ Form::text('titulo', NULL, ['class' => 'form-control', 'required']) }}
				</div>
				<div class="isbn form-group">
					{{ Form::label('isbn', 'ISBN') }}
					{{ Form::text('isbn', NULL, ['class' => 'form-control', 'required']) }}
				</div>
				<div class="editorial form-group">
					{{ Form::label('editorial', 'Editorial') }}
					{{ Form::text('editorial', NULL, ['class' => 'form-control', 'required']) }}
				</div>
				<div class="autor form-group">
					{{ Form::label('autor', 'Autor') }}
					{{ Form::text('autor', NULL, ['class' => 'form-control', 'required']) }}
				</div>
				<div class="coleccion form-group">
					{{ Form::label('coleccion', 'Colección') }}
					{{ Form::text('coleccion', NULL, ['class' => 'form-control', 'placeholder' => 'Elegir Colección']) }}
				</div>
				<div class="edad form-group">
					{{ Form::label('edad', 'Edad') }}
					{{ Form::select('edad', $edadesNombres, NULL, ['class' => 'form-control', 'placeholder' => 'Elegir opción' , 'multiple' => 'multiple', 'name' => 'edad[]']) }}
				</div>
				<div class="genero form-group">
					{{ Form::label('genero', 'Género') }}
					{{ Form::select('genero', $generosNombres, NULL, ['class' => 'form-control', 'placeholder' => 'Elegir opción' , 'multiple' => 'multiple', 'name' => 'genero[]']) }}
				</div>
				<div class="info form-group">
					{{ Form::label('info', 'Libro Informativo') }}
					{{ Form::select('info', $infosNombres, NULL, ['class' => 'form-control', 'placeholder' => 'Elegir opción' , 'multiple' => 'multiple', 'name' => 'info[]']) }}
				</div>

				<div class="distribuidor form-group">
					{{ Form::label('distribuidor', 'Distribuidor') }}
					{{ Form::text('distribuidor', NULL, ['class' => 'form-control', 'placeholder' => 'Elegir Distribudor']) }}
				</div>
			</div>
			<div class="col-md-6 px-5">
				@php
				$anio = 1800;
				$anios = [];
				for ($i=0; $i < 300 ; $i++) {
					$anios[$anio]= $anio;
					$anio++;
				}
				@endphp
				<div class="anio form-group">
					{{ Form::label('anio', 'Año') }}
					{{ Form::select('anio', $anios, NULL, ['class' => 'form-control', 'placeholder' => 'Elegir Año']) }}
				</div>
				<div class="precio form-group">
					{{ Form::label('precio', 'Precio') }}
					{{ Form::text('precio', NULL, ['class' => 'form-control', 'placeholder' => 'Elegir Precio']) }}
				</div>
				<div class="cantidad form-group">
					{{ Form::label('cantidad', 'Stock') }}
					{{ Form::number('cantidad', NULL, ['class' => 'form-control', 'placeholder' => 'Stock']) }}
				</div>
				<div class="tapa form-group">
					{{ Form::label('tapa', 'Tapa') }}<br>
					{{ Form::file('tapa', ['id' => 'tapa']) }}
					<div class="post-thumbnail" style="width:70px; min-height: 105px; margin-top: 20px; background-size: contain"></div>
				</div>
            {{-- <div class="form-check">
                {{ Form::label('destacados1', 'Destacados 1', ['class' => 'mr-4']) }}
                {{Form::checkbox('destacados1', '1', false, ['class' => 'mr-4'])}}

                {{ Form::label('destacados2', 'Destacados 2', ['class' => 'mr-4']) }}
                {{Form::checkbox('destacados2', '1', false, ['class' => 'mr-4'])}}
            </div> --}}
	            <div id="switches">
	            	<div class="d-flex justify-content-between">
	            		<span>pop-up</span>
	            		<label class="switch">
	            			<input name="popup" type="checkbox" value="1">
	            			<span class="slider round"></span>
	            		</label>
	            	</div>
	            	<div class="d-flex justify-content-between mt-5">
	            		<span>destacados 1</span>
	            		<label class="switch">
	            			<input name="destacados1" type="checkbox" value="1">
	            			<span class="slider round"></span>
	            		</label>
	            	</div>
	            	<div class="d-flex justify-content-between mt-5">
	            		<span>destacados 2</span>
	            		<label class="switch">
	            			<input name="destacados2" type="checkbox" value="1">
	            			<span class="slider round"></span>
	            		</label>
	            	</div>

	            	<div class="d-flex justify-content-between mt-5">
	            		<span>activado</span>
	            		<label class="switch">
	            			<input name="activado" type="checkbox" value="1" checked>
	            			<span class="slider round"></span>
	            		</label>
	            	</div>
	            </div> <!-- /#switches -->
        	</div> <!-- /.col -->
    	</div> <!-- /.row -->

	</div> <!-- /.container -->
	<div class="container">
		<div class="row">
			<div class="col-12 px-5">
				<div class="form-group">
					{{Form::label('descripcion', 'Descripción')}}
					{{Form::textarea('descripcion', NULL, ['class' => 'form-control', 'placeholder' => 'descripción...', 'cols'=> 30, 'rows'=> '10'])}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 px-5">
				{{Form::submit('Enviar', ['class' => 'btn btn-primary mt-2'])}}
				<!-- fin . titulo form-group -->
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</section>
@endsection
