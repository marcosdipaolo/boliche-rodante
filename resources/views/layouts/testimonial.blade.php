<section class="testimonial-sec parcial">
    <div class="container">
        <div id="testimonal" class="owl-carousel owl-theme">
            <div class="item">
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum arcu vitae elementum curabitur. Dignissim suspendisse in est ante in nibh. Nec nam aliquam sem et.</h3>
                <div class="box-user">
                    <h4 class="author">Susana Prieto</h4>
                    <span class="country">Villa Crespo</span>
                </div>
            </div>
            <div class="item">
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum arcu vitae elementum curabitur. Dignissim suspendisse in est ante in nibh. Nec nam aliquam sem et.</h3>
                <div class="box-user">
                    <h4 class="author">Susana Prieto</h4>
                    <span class="country">Villa Crespo</span>
                </div>
            </div>
            <div class="item">
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum arcu vitae elementum curabitur. Dignissim suspendisse in est ante in nibh. Nec nam aliquam sem et.</h3>
                <div class="box-user">
                    <h4 class="author">Susana Prieto</h4>
                    <span class="country">Villa Crespo</span>
                </div>
            </div>
            <div class="item">
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum arcu vitae elementum curabitur. Dignissim suspendisse in est ante in nibh. Nec nam aliquam sem et.</h3>
                <div class="box-user">
                    <h4 class="author">Susana Prieto</h4>
                    <span class="country">Villa Crespo</span>
                </div>
            </div>
        </div>
    </div>
    <div class="left-quote">
        <img src="images/left-quote.png" alt="quote">
    </div>
    <div class="right-quote">
        <img src="images/right-quote.png" alt="quote">
    </div>
</section>
