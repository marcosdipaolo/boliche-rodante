<section class="static about-sec pagina">
        <div class="container">
            <h1>Nosotros</h1>
            <div class="img-sec">
                <img src="/images/about.jpg" alt="about">
                <p>Somos feriantes de Literatura Infantil. Somos inquietos y nos gusta pensar que llevamos con nosotros universos que despiertan la imaginación en chicos y grandes. Hacemos delivery por algunos barrios de la ciudad acercando los pedidos a tu casa y también tenemos un espacio en Belgrano que podés visitar con cita previa. </p><p>Podemos sugerirte el mejor regalo para cada ocasión. Nos contás tus inquietudes, temas o gustos y nos acercamos llenos de libros para que puedas elegir. Tenemos toda la colección de La Brujita de Papel, Pequeño Editor, Ojoreja, Chirimbote, Calibroscopio, Del Naranjo, Del Eclipse, Fondo de Cultura Económica, Ekaré, Colihue, Iamiqué, Kalandraka, UnaLuna entre otras...</p><p>En nuestro andar itinerante vamos a Jardines, Escuelas, Centros Culturales e instalamos todos nuestros libros armando una Gran Feria, donde no sólo ofrecemos nuestro asesoramiento con la idea de formar lectores críticos sino que experimentamos la fusión de la buena literatura y las distintas expresiones artísticas en talleres de narración, plástica y movimiento.</p>
            </div>

        </div>
</section>
{{--@include('layouts.testimonial')--}}
<p class="text-center"><a href="{{URL::previous()}}"><button class="btn black">Volver</button></a></p>
