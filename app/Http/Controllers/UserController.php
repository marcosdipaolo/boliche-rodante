<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class UserController extends Controller
{

    /**
     * @param User $user
     * @return Factory|View
     */
    public function show(User $user)
    {
        return view('single-user', compact('user'));
    }

    /**
     * @param User $user
     * @return Factory|View
     */
    public function edit(User $user)
    {
        return view('edit-user', compact('user'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return Factory|View
     * @throws ValidationException
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'direccion' => 'required|string',
            'cp' => 'required|string',
            'ciudad' => 'required|string',
            'provincia' => 'required|string',
            'telefono' => 'required|string',
        ]);

        $user->nombre = $request->nombre ?? null;
        $user->apellido = $request->apellido ?? null;
        $user->direccion = $request->direccion ?? null;
        $user->cp = $request->cp ?? null;
        $user->ciudad = $request->ciudad ?? null;
        $user->provincia = $request->provincia ?? null;
        $user->pais = 'Argentina';
        $user->telefono = $request->telefono ?? null;

        $user->save();

        return view('single-user', compact('user'));
    }
}
