<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIdAndCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('libros', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->string('slug')->after('titulo');
            $table->string('categorias')->after('autor')->default('a:0:{}');
            $table->boolean('destacados1')->after('anio')->default(false);
            $table->boolean('destacados2')->after('anio')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('libros', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropColumn('categorias');
            $table->dropColumn('slug');
            $table->dropColumn('destacados1');
            $table->dropColumn('destacados2');
        });
    }
}
