<section id="single-libro" class="product-sec pagina">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-6 slider-sec text-center">
                <img src="{{resolveImage($libro->img)}}" class="img-fluid">
                @include('layouts.botones-editar-borrar')
            </div>
            <div class="col-md-6 slider-content">
                <h1>{{$libro->titulo}}</h1>
                <ul>
                    <li>
                        <span class="name">Autor</span><span class="clm">:</span>
                        <a href="{{route(Routes\GeneralRoutes::SHOP_BY_AUTHOR, $libro->autor)}}"><span class="">{{$libro->autor}}</span></a>
                    </li>
                    <li>
                        <span class="name">Editorial</span><span class="clm">:</span>
                        <a href="{{route(Routes\GeneralRoutes::SHOP_BY_EDITOR, $libro->editorial)}}"><span class="">{{$libro->editorial}}</span></a>
                    </li>
                    <li>
                        <span class="name">ISBN</span><span class="clm">:</span>
                        <span class="">{{$libro->isbn}}</span>
                    </li>
                    <li>
                        <span class="name">Precio</span><span class="clm">:</span>
                        <span class="price final">${{$libro->precio}}</span>
                    </li>
                    <li>

                    </li>
                @if(!$libro->cantidad > 0)
                    <span class="badge badge-danger">No hay stock</span>
                @endif
                </ul>
                <div class="btn-sec">
                    @if($libro->cantidad > 0)
                    <form action="/carrito" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{$libro->id}}">
                        <button class="btn ">Agregar al carrito</button>
                    </form>
                    @endif
                    <a href="{{URL::previous()}}">
                        <button class="btn black">Volver</button>
                    </a>
                </div>
            </div>
        </div>
        @if($libro->descripcion)
            <div class="mt-5">
                <h4 class="name">Descripción</h4><br>
                <p>{!! paragraphize($libro->descripcion) !!}</p>
            </div>
        @endif
    </div>
</section>
<br>
