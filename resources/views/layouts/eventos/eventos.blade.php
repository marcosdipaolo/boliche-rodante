<!-- . eventos -->
<section id="eventos">
	<div class="container">

		<!-- . row -->
		<div class="row">
		    
		
		<!-- . lista-eventos -->
			<div class="col-md-7 col-lg-8">
				<main class="lista-eventos">


					    @foreach( $eventos as $evento)
					    <!-- . evento -->
					    <article class="evento parcial">
					    	<!-- . imagen -->
					    	<div class="imagen" style="background-image: url('{{Storage::url($evento->img)}}')"></div>
					    	<!-- fin .imagen -->
					    	<br>
					        <a href="/eventos/{{$evento->slug}}"><h4>{{ $evento->titulo }}</h4></a>
					        <br>
					        {!!str_limit($evento->texto, 500)!!}&nbsp;&nbsp;&nbsp;&nbsp;<a href="/eventos/{{$evento->slug}}"><small style="color: #F0563A; font-size: 12px;">ver mas</small></a>
					        <br>
					        <br>
					        <br>
					    </article>
					    <!-- fin .evento -->
					    	
					    @endforeach

						{{ $eventos->links() }}
				</main>
		    </div>
		 <!-- fin .lista-eventos -->


		 	<!-- . sidebar -->
			<div id="aside-cont" class="col-md-5 col-lg-4 pl-5">
		 		@include('layouts.sidebar')
		 	</div>
		 	<!-- fin . sidebar -->
		</div>
				<!-- fin . row -->
	</div>
	<!-- fin .container -->
</section>
<br>

<!-- fin . eventos -->
