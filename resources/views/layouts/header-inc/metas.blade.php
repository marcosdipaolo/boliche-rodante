<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#03a6f3">
@isset($evento)
<!-- Open Graph -->
<meta property="og:title" content="{{ $evento->titulo ?? '' }}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ Config::get('app.url') }}/eventos/{{ $evento->slug ?? '' }}" />
<meta property="og:image" content="{{ Storage::url($evento->img ) ?? '' }}" />
<meta property="og:site_name " content="{{Config::get('app.url')}}" />
<!-- / Open Graph -->
@endisset
<meta name="csrf-token" content="{{ csrf_token() }}">  <!-- csrf token -->