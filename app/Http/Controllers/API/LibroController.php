<?php

namespace App\Http\Controllers\API;

use App\Edad;
use App\Genero;
use App\Http\Controllers\Controller;
use App\Info;
use App\Libro;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

class LibroController extends Controller
{
    /**
     * @return false|LengthAwarePaginator|string
     */
    public function filter()
    {
        $libros = Libro::where('activado', 1)->where('img', '!=', '/images/default.jpg');
        if (request()->edad) {
            $libros = $libros->whereHas('edades', function (Builder $edad) {
                $edad->where('edades.id', request()->edad);
            });
        }
        if (request()->genero) {
            $libros = $libros->whereHas('generos', function (Builder $genero) {
                $genero->where('generos.id', request()->genero);
            });
        }
        if (request()->info) {
            $libros = $libros->whereHas('infos', function (Builder $info) {
                $info->where('infos.id', request()->info);
            });
        }
        if (request()->popup) {
            $libros = $libros->where('popup', 1);
        }
        if (!request()->edad && !request()->info && !request()->genero && !request()->popup) {
            $resp = new \stdClass();
            $resp->data = [];
            return json_encode($resp);
        }
        return $libros->paginate(10);
    }
}
