<?php

namespace Routes;

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\VerificationController;
use Illuminate\Contracts\Routing\Registrar;
use LaravelBA\RouteBinder\Routes;

class AuthRoutes implements Routes
{
    const PASSWORD_REQUEST = 'password.request';
    const PASSWORD_EMAIL = 'password.email';
    const PASSWORD_RESET = 'password.reset';
    const PASSWORD_UPDATE = 'password.update';
    const LOGIN = 'login';
    const LOGOUT = 'logout';
    const REGISTER = 'register';
    const VERIFICATION_NOTICE = 'verification.notice';
    const VERIFICATION_RESEND = 'verification.resend';
    const VERIFICATION_VERIFY = 'verification.verify';

    public function addRoutes(Registrar $router)
    {
        $router->group(['middleware' => 'web'], function() use ($router){
            $router->post('logout', ['uses' => LoginController::class . '@logout', 'as' => self::LOGOUT]);
            $router->group(['middleware' => 'auth'], function () use ($router){
                $router->get('email/verify', ['uses' => VerificationController::class . '@show', 'as' => self::VERIFICATION_NOTICE]);
                $router->group(['middleware' => 'throttle:6,1'], function () use ($router){
                    $router->get('email/resend', ['uses' => VerificationController::class .'@resend', 'as' => self::VERIFICATION_RESEND]);
                    $router->group(['middleware' => 'signed'], function () use ($router){
                        $router->get('email/verify/{id}', ['uses' => VerificationController::class . '@verify', 'as' => self::VERIFICATION_VERIFY]);
                    });
                });
            });
            $router->group(['middleware' => 'guest'], function () use ($router){
                $router->get('login', ['uses' => LoginController::class . '@showLoginForm', 'as' => self::LOGIN]);
                $router->post('login', ['uses' => LoginController::class . '@login']);
                $router->get('password/reset', ['uses' => ForgotPasswordController::class . '@showLinkRequestForm', 'as' => self::PASSWORD_REQUEST ]);
                $router->post('password/email', ['uses' => ForgotPasswordController::class . '@sendResetLinkEmail', 'as' => self::PASSWORD_EMAIL]);
                $router->get('password/reset/{token}', ['uses' => ResetPasswordController::class . '@showResetForm', 'as' => self::PASSWORD_RESET]);
                $router->post('password/reset', ['uses' => ResetPasswordController::class . '@reset', 'as' => self::PASSWORD_UPDATE]);
                $router->get('register', ['uses' => RegisterController::class . '@showRegistrationForm', 'as' => self::REGISTER]);
                $router->post('register', ['uses' => RegisterController::class . '@register']);
            });
        });
    }
}
