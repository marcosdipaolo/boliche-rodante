<section class="offers-sec parcial" style="background:url(images/offers.jpg); background-repeat: no-repeat;">
    <div class="cover"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="detail">
                    <h3>Alguna oferta del 50% OFF</h3>
                    <h6>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin congue, nibh ut molestie egestas. </h6>
                    <a href="/shop" class="btn blue-btn">Todos los libros</a>
                    <span class="icon-point percentage">
                            <img src="images/precentagae.png" alt="">
                        </span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="detail">
                    <h3>Por mas de $500 con beneficios!!</h3>
                    <h6>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin congue, nibh ut molestie egestas. </h6>
                    <a href="/shop" class="btn blue-btn">Todos los libros</a>
                    <span class="icon-point amount"><img src="images/amount.png" alt=""></span>
                </div>
            </div>
        </div>
    </div>
</section>