<div class="container">
	<div class="row justify-content-center">
		<a href="/admin/eventos"><button class="btn mb-5">
	        eventos
	    </button>
		</a>
		<a href="/actualizar-precios"><button class="btn mb-5">
		        precios y stock
		    </button>
		</a>
		<a href="/compras"><button class="btn mb-5">
		        compras
		    </button>
		</a>
		<a href="/libros"><button class="btn mb-5">
		      Libros
		    </button>
		</a>
		<a href="/libros/create"><button class="btn mb-5">
		        Crear Libro
		    </button>
		</a>
		<a href="/eventos/create"><button class="btn mb-5">
		        Crear Evento
		    </button>
		</a>	
	</div>
</div>
