<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Info
 *
 * @property int $id
 * @property string $info
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Libro[] $libros
 * @property-read int|null $libros_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info whereInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Info extends Model
{
    /**
     * @return BelongsToMany
     */
    public function libros()
    {
    	return $this->belongsToMany(Libro::class);
    }
}
