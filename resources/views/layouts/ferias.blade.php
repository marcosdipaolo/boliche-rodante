<section id="ferias" class="ferias pagina">
	<div class="container">
        <p>
            <strong>Boliche Rodante</strong> es un proyecto familiar, que nace en el año 2013. Somos amantes de los libros y nos gusta acercar la lectura a los más pequeños abriendo posibles universos, emociones, sensaciones y conocimientos.
        </p><br><p>
        Somos feriantes de <strong>Literatura Infantil y Juvenil</strong> y en nuestro andar itinerante vamos a Jardines, Escuelas, Centros Culturales, armamos una <strong>Gran Feria</strong> ofreciendo nuestro asesoramiento. Se trata de una experiencia participativa e interactiva para toda la comunidad educativa, con enorme variedad de títulos seleccionados especialmente.
        </p><br><p>
        Proponemos actividades que <strong>promueven la lectura</strong> a partir del juego, la imaginación y el disfrute. Un recorrido sensorial y de libre exploración buscando despertar la curiosidad de cada niño en contacto directo con los libros.
        </p><br><p>
        El material que llevamos, de editoriales nacionales e internacionales, es elegido con cuidadosa atención y criterio, en diversidad de géneros y estilos. Tenemos toda la colección de La Brujita de Papel, Pequeño Editor, Ojoreja, Chirimbote, Calibroscopio, Del Naranjo, Del Eclipse, Fondo de Cultura Económica, Ekaré, Colihue, Iamiqué, Kalandraka, UnaLuna, Blume, Taschen, Limonero, Periplo, Niño Editor, Colihue, Norma, Pocas Nueces, La Bohemia, SM, Loqueleo, Zorro Rojo, Coco Book, Tragaluz, Tinkuy, entre otras editoriales.
        </p><br><p>
        ﻿​La feria tiene una duración de entre tres y cinco días. Brindamos asesoramiento e información sobre los libros exhibidos, de acuerdo a las experiencias e intereses de cada lector. Los chicos y chicas podrán, posteriormente, adquirir libros si lo desean, pero <strong>no es obligatorio comprar. Todos los chicos tienen acceso a la lectura</strong> y a la posibilidad de participar de las actividades que acompañan a la feria.
        </p><br><p>
        Además, ofrecemos talleres de narración, de diseño y experimentación con susurradores, propuestas plásticas, juegos literarios y charlas con autores, poniendo en relación la literatura con distintas expresiones artísticas.
        </p><br><p>
        El evento <strong>no supone costo alguno para la institución</strong> y no interfiere con la dinámica de trabajo diaria. Las actividades, a cargo de nuestro equipo, se pautan previamente para coordinar los horarios de los distintos grupos, sin implicar trabajo extra para docentes ni directivos. Cualquier sugerencia de parte de la institución en cuanto al contenido que vienen trabajando o desean profundizar, es bienvenida. Una vez finalizado el evento, la comunidad escolar recibe una <strong>donación de libros</strong> de variada temática y para todos los niveles.
        </p><br><p>
        Los requerimientos para la realización del evento son un espacio amplio, cerrado y seguro que pueda ser visitado por niños, niñas, docentes y padres, y que permita mantener los libros resguardados durante las jornadas en que permanezca la feria en la escuela. Y la colaboración con la difusión previa sobre la realización de la feria, a través de material informativo que entregaremos con anticipación.
        </p>
        <br>
	</div>
</section>
