<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

class CommandMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->bearerToken() === config('app.api_command_token')) {
            return $next($request);
        }
        return new JsonResponse(['message' => 'Unauthorized'], 401);
    }
}
