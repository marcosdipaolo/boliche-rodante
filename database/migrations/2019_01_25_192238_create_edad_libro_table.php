<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdadLibroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edad_libro', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('edad_id');
            $table->foreign('edad_id')->references('id')->on('edades')->onDelete('cascade');
            $table->unsignedInteger('libro_id');
            $table->foreign('libro_id')->references('id')->on('libros')->onDelete('cascade');
            $table->timestamps();
        });
        DB::table('edades')->insert([
        	['edad' => 'Libros para bebes hasta 2 años'],
        	['edad' => 'Me gusta que me lean (3-4 años)'],
        	['edad' => 'Descubriendo las letras (4-5 años)'],
        	['edad' => 'Empiezo a leer (Desde 6 años)'],
        	['edad' => 'Desde 8 años'],
        	['edad' => 'Desde 10 años'],
        	['edad' => 'Desde 12 años'],
        	['edad' => 'Adultos']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edad_libro');
    }
}
