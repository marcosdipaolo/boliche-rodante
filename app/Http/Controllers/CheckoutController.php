<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Checkout;
use App\Libro;
use App\Post;
use App\Traits\PaginateItems;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use MercadoPago\Item;
use MercadoPago\Payer;
use MercadoPago\Preference;

class CheckoutController extends Controller
{
    use PaginateItems;

    /**
     *
     */
    public function __construct()
    {
        $this->middleware('verified');
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $preference = $this->create();
        $cart_items = \Session::has('cart') ? (\Session::get('cart')->items ?? []) : [];
        $recoms = Libro::where('destacados2', '=', '1')->take(4)->orderBy('updated_at', 'DESC')->get();
        $recents = Libro::where('destacados1', '=', '1')->take(4)->orderBy('updated_at', 'DESC')->get();
        $sidebar_posts = Post::take(4)->get();
        return view('checkout', [
            'sidebar_posts' => $sidebar_posts,
            'recents' => $recents,
            'recoms' => $recoms,
            'cart_items' => $cart_items,
            'preference' => $preference,
        ]);
    }

    public function create()
    {
        \MercadoPago\SDK::setAccessToken(config('mercadopago.access_token'));
        /** @var Cart $cart */
        $cart = session('cart');
        $items = $cart instanceof Cart ? collect($cart->items) : collect([]);

        $preference = new Preference();
        $mp_items = new Collection();
        $items->each(function ($item) use ($mp_items) {
            /** @var Libro $libro */
            $libro = $item['item'];
            $qty = $item['cant'];

            $mp_item = new Item();
            $mp_item->id = $libro->id;
            $mp_item->title = $libro->titulo;
            $mp_item->description = $libro->descripcion;
            $mp_item->quantity = $qty;
            $mp_item->currency_id = "ARS";
            $mp_item->unit_price = $libro->precio;
            $mp_items->push($mp_item);
        });
        // Payer

        $payer = new Payer();
        $payer->name = auth()->user()->nombre;
        $payer->surname = auth()->user()->apellido;
        $payer->email = auth()->user()->email;
        $payer->date_created = auth()->user()->created_at;
        $payer->phone = array(
            "area_code" => "",
            "number" => auth()->user()->telefono
        );
        $payer->identification = array(
            "type" => "ID",
            "number" => auth()->user()->id,
        );
        $payer->address = array(
            "street_name" => auth()->user()->direccion,
            "zip_code" => auth()->user()->cp,
        );
        //
        $preference->items = $mp_items->toArray();
        $preference->payer = $payer;
        try {
            $preference->save();
            return $preference;
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Hubo un problema generando la orden de pago');
        }
    }

    /**
     * @param $value
     * @param $order_id
     */
    public function store($value, $order_id)
    {

    }

    /**
     * @return Factory|View
     */
    public function adminIndex()
    {
        $checkouts = [];
        $checkoutsCollection = Checkout::all()->map(function(Checkout $checkout){
            $booksInfo = json_decode($checkout->cart, true);
            $data = [
                'id' => $checkout->id,
                'user' => $checkout->user,
                'sent' => $checkout->sent,
                'totalQty' => $booksInfo['totalQty'],
                'totalPrice' => $booksInfo['totalPrice'],
                'sale_date' => $checkout->created_at,
                'items' => [],
            ];
            if (isset($booksInfo['items'])) {
                foreach($booksInfo['items'] as $id => $bookInfo) {
                    array_push($data['items'], [
                        'book' => Libro::find($id),
                        'unit_price' => $bookInfo['item']['precio'],
                        'qty' =>  $bookInfo['cant'],
                    ]);
                }
            }
            return $data;
        });
        $checkoutsArray = $this->getPaginatedItems(collect($checkoutsCollection), 50, "/compras");

        return view('editar.checkouts', compact('checkoutsArray'));
    }

    /**
     * @param Request $request
     * @param Checkout $compra
     * @return Checkout
     */
    public function sent(Request $request, Checkout $compra)
    {
        $request->validate([
            'sent' => 'boolean'
        ]);
        $compra->sent = request('enviado');
        $compra->save();
        return $compra;
    }
}
