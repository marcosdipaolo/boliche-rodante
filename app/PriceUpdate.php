<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\PriceUpdate
 *
 * @property int $id
 * @property string $editorial
 * @property string $xls
 * @property int $success
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceUpdate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceUpdate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceUpdate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceUpdate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceUpdate whereEditorial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceUpdate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceUpdate whereSuccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceUpdate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceUpdate whereXls($value)
 * @mixin Eloquent
 */
class PriceUpdate extends Model
{
	//
}
