@extends('layouts.master')
@section('content')

<span id="body_class" data-class="libros" class="d-none"></span>
@if(! $_GET)
@include('layouts.filters')
@endif
<!-- . libros -->
<div id="libros">
	@include('layouts.recomendados')
	@include('layouts.muy-recom')
</div>		
<!-- fin . libros -->

<a id="all-books"></a>
<div class="btn-sec" style="@if($_GET)display: none;@endif margin: 30px 0; text-align: center">
    <a href="#all-books" class="btn gray-btn">Todos los libros</a>
</div>

@include('layouts.all-books')

@endsection
