<section class="features-sec clearfix parcial">
    <div class="container">
        <div class="row">
        <ul class="clearfix">
            <li>
                <span class="icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                <h3>Comprá seguro</h3>
                <h5>Lorem Ipsum</h5>
                <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</h6>
            </li>
            <li>
                <span class="icon return"><i class="fa fa-reply-all" aria-hidden="true"></i></span>
                <h3>Devoluciones - 30 días</h3>
                <h5>Lorem Ipsum</h5>
                <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</h6>
            </li>
            <li>
                <span class="icon chat"><i class="fa fa-comments" aria-hidden="true"></i></span>
                <h3>Soporte 24/7</h3>
                <h5>Lorem Ipsum</h5>
                <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</h6>
            </li>
        </ul>
        </div>
    </div>
</section>