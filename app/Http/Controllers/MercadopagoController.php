<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Checkout;
use App\Libro;
use App\MPPreference;
use App\User;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Routes\GeneralRoutes;

class MercadopagoController extends Controller
{
    /**
     * MercadopagoController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * @return Application|RedirectResponse|Redirector
     */
    public function mercadopago()
    {
        /** @var MPPreference $mp_preference */
        $mp_preference = MPPreference::create(request()->all());
        /** @var Cart $cart */
        $cart = session()->get('cart');
        switch ($mp_preference->payment_status) {
            case('approved'):
                $this->updateBooksStock($cart);
                $this->createCheckout();
                $this->emptyCart();
                return redirect('/')->withSuccess('La compra se realizó con éxito');
            case('pending'):
                $this->emptyCart();
                return redirect()->route(GeneralRoutes::HOME)->withSuccess('La compra quedó en estado "PENDIENTE"');
            case('in_process'):
                $this->emptyCart();
                return redirect('/')->withSuccess('La compra está en proceso.');
            case('failure'):
                return redirect('/')->withErrors('Hubo un problema procesando la compra.');
            default:
                return redirect('/')->withSuccess('La compra se realizó con éxito');
        }
    }

    private function updateBooksStock(Cart $cart): void
    {
        foreach ($cart->items as $item) {
            /** @var Libro $book */
            $book = $item['item'];
            $book->cantidad -= $item['cant'];
            $book->save();
        }
    }

    private function emptyCart(): void
    {
        session(['cart' => new Cart(null)]);
    }

    private function createCheckout()
    {
        if(session()->has('cart')) {
            $cart = session()->get('cart');
            /** @var User $user */
            $user = auth()->user();
            Checkout::create([
                'user_id' => $user->id,
                'cart' => json_encode($cart),
                'sent' => false
            ]);
        }
    }
}
