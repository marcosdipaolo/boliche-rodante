@extends('editar.app')

@section('content')
<!-- . editar-evento -->
<section class="editar-evento">
	@include('layouts.errors')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-9">
				<h4>Crear evento</h4>
				<br><br>
			</div>
		</div>
		<div class="row justify-content-center">
			<br><br>
			<div class="col-md-9">
				@php
				$quill = true;
				@endphp
				{!! Form::open(['action' => [ App\Http\Controllers\PostController::class . '@store'], 'files' => true, 'id' => 'edit-post']) !!}
				<!-- . titulo form-group -->
				<div class="titulo form-group">
					{{ Form::label('titulo', 'Título') }}
					{{ Form::text('titulo', NULL, ['class' => 'form-control']) }}
				</div>
				<div class="texto form-group">
					{{ Form::label('texto', 'Texto') }}
				</div>
				<div id="textarea" name="texto"></div>
				<input type="hidden" name="texto" id="texto">
				<input type="hidden" name="user_id" id="user_id" value"{{auth()->user()->id}}">
				<br>
				<br>
				{{Form::hidden('user_id', auth()->user()->id)}}
				<div class="img form-group text-center">
					{{ Form::file('img', ['id' => 'tapa']) }}
					<div class="post-thumbnail d-inline-block" style="width: 150px;min-height: 105px"></div>
				</div>
				<br><br>
			</div>
		</div> <!-- /.row -->

		<p class="text-center">{{Form::submit('Enviar', ['class' => 'btn btn-primary mt-2 mx-5', 'id' => 'submit-post'])}}
		</p>
		<!-- fin . titulo form-group -->
		{!! Form::close() !!}
	</div>
</section>
<!-- fin . editar-evento -->
@endsection
