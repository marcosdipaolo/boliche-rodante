 <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('genero');
            $table->timestamps();
        });
        DB::table('generos')->insert([
        	['genero' => 'Historietas y novela gráfica'],
        	['genero' => 'Sagas'],
        	['genero' => 'Cuentos tradicionales, clásicos y leyendas'],
        	['genero' => 'Poesía'],
        	['genero' => 'Fantasía'],
        	['genero' => 'Narrativa Juvenil']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generos');
    }
}
