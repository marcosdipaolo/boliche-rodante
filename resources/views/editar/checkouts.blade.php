@extends('editar.app')

@section('content')
@php
    $pagina = isset($_GET['page']) ? "?page={$_GET['page']}" : '';
@endphp
<!-- . compras -->
<section id="compras">
	@include('editar.botones')
    <div class="container">
        <div class="row justify-content-center">
    		<div class="col-lg-12">
    			{{ $checkoutsArray->links() }}
    			<br>
    			<table class="table table-sm">
    				<thead>
    					<tr>
                            <th>#</th>
    						<th style="width: 15%"><a class="orden" href="/compras{{$pagina}}">Usuario <i class="fas fa-caret-down"></i></a></th>
    						<th>E-mail</th>
                            <th style="width: 50%">Libros</th>
                            <th>Precio Total</th>
    						<th ><a class="orden" href="/compras/orden/editorial{{$pagina}}">Fecha <i class="fas fa-caret-down"></i></a></th>
    						<th style="width: 10%"><a href="/compras/orden/precio{{$pagina}}">Enviado <i class="fas fa-caret-down"></i></a></th>
    					</tr>
    				</thead>
    				<tbody>
    					@foreach($checkoutsArray as $checkout)
    					<tr>
                            <td>{{isset($_GET['page']) ? $loop->iteration + (($_GET['page'] - 1 ) * 50) : $loop->iteration}}</td>
    						<td>{{$checkout['user']->nombre . ' '. $checkout['user']->apellido}}</td>
    						<td>{{$checkout['user']->email}}<br>{{$checkout['user']->direccion}}</td>
    						@php
    							$libros_str = '';
    							$libros_arr = [];
    							foreach ($checkout['items'] as $data) {
    								$libros_arr[] = "<strong>{$data['book']->titulo}</strong> (cantidad: <strong>{$data['qty']}</strong>, precio unitario: <strong>\${$data['book']->precio}.-</strong>)";
    							}
    							$libros_str = '- ' . implode($libros_arr, ',<br/>- ');
    						@endphp
                            <td>{!!$libros_str!!}</td>
                            <td><strong>${{$checkout['totalPrice']}}.-</strong></td>
    						<td>{{$checkout['sale_date']->diffForHumans()}}</td>
    						<td><form method="post" action="/compra/edit/{{$checkout['id']}}">
    							@csrf
    							@method('PUT')
    							<input data-compra="{{$checkout['id']}}" class="enviado" type="checkbox" name="enviado" value="1" @if($checkout['sent']) checked @endif></form>
                                <div class="alert alert-success" style="width: 400px"></div>
                                <div class="alert alert-danger">Ocurrió un error.</div>
    						</td>
    					</tr>
    					@endforeach
    				</tbody>
    			</table>
    			{{ $checkoutsArray->links() }}
    		</div>
    	</div>
    </div>
</section>
<!-- fin . compras -->

@endsection
