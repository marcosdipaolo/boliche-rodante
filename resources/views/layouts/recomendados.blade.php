@if(count($recomendados))
<!--  Recomendados  -->
<section class="recomended-sec parcial" @if($_GET) style="display: none;" @endif>
    <div class="container">
        <div class="title">
            <h2>{{ __('Libros recomendados')}}</h2>
            <hr>
        </div>
        <div class="row fila justify-content-center">
            @foreach ($recomendados as $recomendado)
                <div class="col-lg-3 col-md-6 item-wrapper mt-4">
                    <div class="item">
                        <div class="item-img">
                            <img src="@if($recomendado->img){{Storage::url($recomendado->img)}}@else {{asset('/images/default.jpg')}} @endif" alt="img">
                        </div>
                        <div class="item-content">
                            @if(!$recomendado->cantidad > 0)
                                <div class="position-relative mb-1">
                                    <span class="badge badge-danger">No hay stock</span>
                                </div>
                            @endif
                            <h3>{{$recomendado->titulo}}</h3>
                            <h6><span class="price">${{$recomendado->precio}}</span> / <a href="#">Ver descripción</a></h6>
                        </div>
                        <div class="hover">
                            <a href="/libros/{{$recomendado->slug}}">
                                <span><i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>
@endif
