@extends('editar.app')

@section('content')
<!-- . editar-evento -->
<section class="editar-evento">
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
        <h4>Editamos el evento <strong><em>{{$evento->titulo}}</em></strong></h4>
        <br><br>
            <div class="col-md-9">
                @php
                    $quill = true;
                @endphp
                {!! Form::open(['action' => [ App\Http\Controllers\PostController::class .  '@update', $evento->slug], 'method' => 'PUT', 'files' => true, 'id' => 'edit-post']) !!}
                <!-- . titulo form-group -->
                <div class="titulo form-group">
                    {{ Form::label('titulo', 'Título') }}
                    {{ Form::text('titulo', $evento->titulo, ['class' => 'form-control']) }}
                </div>
                <div class="texto form-group">
                    {{ Form::label('texto', 'Texto') }}
                </div>
                <div id="textarea" name="texto">{!! $evento->texto !!}</div>
                <input type="hidden" name="texto" id="texto">
                <br>
                <br>
                {{Form::hidden('user_id', auth()->user()->id)}}
                <div class="img form-group text-center">
                    {{ Form::file('img', ['id' => 'tapa']) }}
                    <div class="post-thumbnail d-inline-block" style="background-image: url({{Storage::url($evento->img)}}); width: 150px;min-height: 105px"></div>
                </div><br><br>
            </div></div>

            <p class="text-center">{{Form::submit('Enviar', ['class' => 'btn btn-primary mt-2 mx-5', 'id' => 'submit-post'])}}</p>
                <!-- fin . titulo form-group -->
                {!! Form::close() !!}
        </div>
    </div>
</section>
<!-- fin . editar-evento -->
@endsection
