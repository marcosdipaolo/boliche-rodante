<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNombreApellidoPaisCiudadDireccionCPTelefono extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('nombre')->after('name')->nullable();
            $table->string('apellido')->after('nombre')->nullable();
            $table->string('direccion')->after('apellido')->nullable();
            $table->string('cp')->after('direccion')->nullable();
            $table->string('ciudad')->after('cp')->nullable();
            $table->string('provincia')->after('ciudad')->nullable();
            $table->string('pais')->after('provincia')->nullable();
            $table->string('telefono')->after('pais')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('nombre');
            $table->dropColumn('apellido');
            $table->dropColumn('direccion');
            $table->dropColumn('ciudad');
            $table->dropColumn('provincia');
            $table->dropColumn('pais');
            $table->dropColumn('cp');
            $table->dropColumn('telefono');
        });
    }
}
