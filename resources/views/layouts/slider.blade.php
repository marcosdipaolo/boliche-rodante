<section class="slider clearfix">
    <div class="container">
        <div id="owl-demo" class="owl-carousel owl-theme">
        <!-- comienza item slider -->
            <div class="item">
                <div class="slide">
                    <img src="{{asset('images/portada-libros.jpg')}}" alt="slide1">
                    <div class="content">
                        <div class="title">
                            <h3>Bienvenidos a Boliche Rodante</h3>
                            <h5>Vení a curiosear nuestro catálogo online!!!</h5>
                            <a href="/shop" class="btn">Buscar</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- termina item slider -->
        @foreach ($eventos as $evento)
            <!-- comienza item slider -->
            <div class="item">
                <div class="slide">
                    <img src="{{Storage::url($evento->img)}}" alt="slide{{$loop->iteration + 1}}">
                    <div class="content">
                        <div class="title">
                            <h3>{{$evento->titulo}}</h3>
                            <h5>{{ str_limit( strip_tags($evento->texto), 50 ) }}</h5>
                            <a href="/eventos/{{$evento->slug}}" class="btn">Ver evento</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- termina item slider -->
        @endforeach

        </div>
    </div>
</section>
