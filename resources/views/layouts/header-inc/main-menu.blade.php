<div class="main-menu">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light">
			<a style="margin: 2px 0 0 80px;" class="navbar-brand" href="/"><img class="logo" src="/images/logo-animado-200px.gif">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="navbar-item active">
						<a href="/" class="nav-link">Home</a>
					</li>
					<li class="navbar-item">
						<a href="/shop" class="nav-link">Libros</a>
					</li>
					<li class="navbar-item">
						<a href="/about" class="nav-link">Nosotros</a>
					</li>
                    @if(count($sidebar_posts))
					<li class="navbar-item">
						<a href="/eventos" class="nav-link">Eventos</a>
					</li>
                    @endif
					<li class="navbar-item">
						<a href="/ferias" class="nav-link">Ferias</a>
					</li>
				</ul>
				<div class="cart my-2 my-lg-0">
					<a href="/carrito">
						<span>
							<i class="fas fa-shopping-cart" aria-hidden="true"></i>
						</span>
						<span class="quntity">{{Session::has('cart') ? Session::get('cart')->totalQty : 0}}
						</span>
					</a>
				</div>
				<form id="search" method="GET" action="/search" class="form-inline my-2 my-lg-0">
					<input class="form-control mr-sm-2" type="text" placeholder="Buscar..." name="query" aria-label="Search">
					<span id="lupa" class="fa fa-search"></span>
				</form>
			</div>
		</nav>
	</div> <!-- /.container -->
</div> <!-- /.main-menu -->
