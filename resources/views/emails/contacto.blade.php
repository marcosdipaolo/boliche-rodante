@component('mail::message')
# Contacto de Usuario - Bolicherodante.com

El usuario {{$user->name}} se puso en contacto.

<strong>E-mail:</strong> {{$user->email}} <br>
<strong>Mensaje:</strong> {{$mensaje}} <br>

@component('mail::button', ['url' => "mailto:{$user->email}"])
Contestar
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
