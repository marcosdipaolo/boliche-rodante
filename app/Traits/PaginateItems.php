<?php

namespace App\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

trait PaginateItems
{
    public function getPaginatedItems(Collection $collection, int $perPage, string $queryString): LengthAwarePaginator
    {
        /* *************************************************************************************************** */
        $currentPage = LengthAwarePaginator::resolveCurrentPage(); // Get current page form url e.x. &page=1
        $itemCollection = $collection; // Create a new Laravel collection from the array data
        $pages = $perPage; // Define how many items we want to be visible in each page
        $currentPageItems = $itemCollection->slice(($currentPage * $pages) - $pages, $pages)->all(); // Slice the collection to get the items to display in current page
        $paginator= new LengthAwarePaginator($currentPageItems , count($itemCollection), $pages); // Create our paginator and pass it to the view
        $paginator->setPath($queryString); // set url path for generated links
        /* ******************************************************************************************************** */
        return $paginator;
    }
}
