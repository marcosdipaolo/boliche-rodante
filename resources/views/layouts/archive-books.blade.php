<section class="all-books pagina mb-5">

    <div class="container">
        {{$libros->links()}}
        <br>
        <br>
        <h3 class="text-left">
            @isset($editorial)
                Editorial: {{ucfirst(trim($editorial))}}
            @endisset
            @isset($autor)
                Autor: {{ucfirst(trim($autor))}}
            @endisset
        </h3>
        <div class="row mt-5">
            @foreach($libros as $libro)
                <div class="col-lg-2 col-md-3 col-sm-4 mt-4">
                    <div class="item __{{$libro->id}}">
                        <div class="item-img">
                            <a href="/libros/{{$libro->slug}}"><img src="{{resolveImage($libro->img)}}" alt="img"></a>
                        </div>
                        <div class="item-info">
                            <h3><a href="/libros/{{$libro->slug}}">{{$libro->titulo}}</a></h3>
                            <h6><span class="price">${{$libro->precio}}.-</span> / <a href="#" onclick="event.preventDefault(); document.querySelector('form#comprarAhora{{$loop->iteration}}').submit()" style="border: none">Comprar ahora</a>
                            </h6>
                            <form action="/carrito" method="POST" id="comprarAhora{{$loop->iteration}}">
                                @csrf
                                <input type="hidden" name="id" value="{{$libro->id}}">
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        {{$libros->links()}}
    </div>
</section>
