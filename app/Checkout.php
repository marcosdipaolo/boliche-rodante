<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


class Checkout extends Model
{
protected $guarded = [];
    /**
     * @return BelongsTo
     */
  public function user()
  {
  	return $this->belongsTo(User::class);
  }
}
