<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class CacheController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function routesCache()
    {
        try {
            \Artisan::call('route:cache');
            return new JsonResponse(['message' => 'ok', 200]);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage(), $e->getCode()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function routesClear()
    {
        try {
            \Artisan::call('route:clear');
            return new JsonResponse(['message' => 'ok', 200]);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage(), $e->getCode()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function configCache()
    {
        try {
            \Artisan::call('config:cache');
            return new JsonResponse(['message' => 'ok', 200]);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage(), $e->getCode()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function clearCache()
    {
        try {
            \Artisan::call('cache:clear');
            return new JsonResponse(['message' => 'ok', 200]);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage(), $e->getCode()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function viewCache()
    {
        try {
            \Artisan::call('view:cache');
            return new JsonResponse(['message' => 'ok', 200]);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage(), $e->getCode()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function clearView()
    {
        try {
            \Artisan::call('view:clear');
            return new JsonResponse(['message' => 'ok', 200]);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage(), $e->getCode()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function migrate()
    {
        try {
            \Artisan::call('migrate', [
                '--path' => 'database/migrations',
                '--force' => true,
            ]);
            return new JsonResponse(['message' => 'ok', 200]);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage(), $e->getCode()]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function dumpAutoload()
    {
        try {
            \Artisan::call('dump-autoload');
            return new JsonResponse(['message' => 'ok', 200]);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => 'failed', 500]);
        }
    }
}
