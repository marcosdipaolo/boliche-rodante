<?php

namespace Routes;

use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\LibroController;
use App\Http\Controllers\PriceUpdateController;
use Illuminate\Contracts\Routing\Registrar;
use LaravelBA\RouteBinder\Routes;

class AdminRoutes implements Routes
{
    const UPDATE_PRICES_INDEX = 'actualizar.precios.get';
    const UPDATE_PRICES_STORE = 'actualizar.precios.post';

    public function addRoutes(Registrar $router)
    {
        $router->group(['middleware' =>'web'], function () use ($router) {
//            $router->get('/precios/{libro}', function(){ return view('precios');})->middleware('admin');
//            $router->get('/precios', LibroController::class . '@indexPrecios');
            $router->put('/precios/{libro}', ['uses' => LibroController::class . '@editarPrecio']);
            $router->get('/actualizar-precios',[ 'as' => self::UPDATE_PRICES_INDEX , 'uses' => PriceUpdateController::class . '@index']);
            $router->post('/actualizar-precios', ['uses' => PriceUpdateController::class . '@store', 'as' => self::UPDATE_PRICES_STORE]);
            $router->get('/admin/eventos', ['uses' => PostController::class . '@index']);
        });
    }
}
