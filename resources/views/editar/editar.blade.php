@extends('editar.app')

@section('content')
<section id="editar-libro">
	<div class="container">
		@include('layouts.errors')
		<br>
		<h4>Editamos el libro <strong><em>{{$libro->titulo}}</em></strong></h4><br>
		<div class="row">
			<div class="col-md-6 px-5">
				{!! Form::open(['action' => [ App\Http\Controllers\LibroController::class .  '@update', $libro->slug], 'method' => 'PUT', 'files' => true]) !!}
				<!-- . titulo form-group -->
				<div class="titulo form-group">
					{{ Form::label('titulo', 'Título') }}
					{{ Form::text('titulo', $libro->titulo, ['class' => 'form-control']) }}
				</div>
				<div class="isbn form-group">
					{{ Form::label('isbn', 'ISBN') }}
					{{ Form::text('isbn', $libro->isbn, ['class' => 'form-control']) }}
				</div>
				<div class="editorial form-group">
					{{ Form::label('editorial', 'Editorial') }}
					{{ Form::text('editorial', $libro->editorial, ['class' => 'form-control']) }}
				</div>
				<div class="autor form-group">
					{{ Form::label('autor', 'Autor') }}
					{{ Form::text('autor', $libro->autor, ['class' => 'form-control']) }}
				</div>
				<div class="coleccion form-group">
					{{ Form::label('coleccion', 'Colección') }}
					{{ Form::text('coleccion', $libro->coleccion, ['class' => 'form-control', 'placeholder' => 'Elegir Colección']) }}
				</div>
				<div class="edad form-group">
					{{ Form::label('edad', 'Edad') }}
					<select class="form-control" id="edad", name="edad[]" multiple>
						{{-- foreach 1 --}}
						@foreach($edades as $edad)
							@php
								$boolEdad = false;
							@endphp
							{{-- foreach 2 --}}
							@foreach($libro->edades as $libroEdad)
								@if($libroEdad->id === $edad->id)
									@php
										$boolEdad = true;
									@endphp
								@endif
							@endforeach
							{{--  end foreach 2 --}}
							<option value="{{$edad->id}}" {{$boolEdad ? 'selected' : ''}}>{{$edad->edad}}</option>
						@endforeach
						{{-- end foreach1 --}}
					</select>
				</div>
				<div class="edad form-group">
					{{ Form::label('genero', 'Género') }}
					<select class="form-control" id="genero", name="genero[]" multiple>
						{{-- foreach 1 --}}
						@foreach($generos as $genero)
							@php
								$boolGenero = false;
							@endphp
							{{-- foreach 2 --}}
							@foreach($libro->generos as $libroGeneros)
								@if($libroGeneros->id === $genero->id)
									@php
										$boolGenero = true;
									@endphp
								@endif
							@endforeach
							{{--  end foreach 2 --}}
							<option value="{{$genero->id}}" {{$boolGenero ? 'selected' : ''}}>{{$genero->genero}}</option>
						@endforeach
						{{-- end foreach1 --}}
					</select>
				</div>
				<div class="info form-group">
					{{ Form::label('info', 'Libro informativo') }}
					<select class="form-control" id="info", name="info[]" multiple>
						{{-- foreach 1 --}}
						@foreach($infos as $info)
							@php
								$boolInfo = false;
							@endphp
							{{-- foreach 2 --}}
							@foreach($libro->infos as $libroInfo)
								@if($libroInfo->id === $info->id)
									@php
										$boolInfo = true;
									@endphp
								@endif
							@endforeach
							{{--  end foreach 2 --}}
							<option value="{{$info->id}}" {{$boolInfo ? 'selected' : ''}}>{{$info->info}}</option>
						@endforeach
						{{-- end foreach1 --}}
					</select>
				</div>

			</div>
			<div class="col-md-6 px-5">
				<div class="distribuidor form-group">
					{{ Form::label('distribuidor', 'Distribuidor') }}
					{{ Form::text('distribuidor', $libro->distribuidor, ['class' => 'form-control', 'placeholder' => 'Elegir Distribudor']) }}
				</div>
				@php
				$anio = 1800;
				$anios = [];
				for ($i=0; $i < 300 ; $i++) {
					$anios[$anio]= $anio;
					$anio++;
				}
				@endphp
				<div class="anio form-group">
					{{ Form::label('anio', 'Año') }}
					{{ Form::select('anio', $anios, $libro->anio, ['class' => 'form-control', 'placeholder' => 'Elegir Año']) }}
				</div>
				<div class="precio form-group">
					{{ Form::label('precio', 'Precio') }}
					{{ Form::text('precio', $libro->precio, ['class' => 'form-control', 'placeholder' => 'Elegir Precio']) }}
				</div>
				<div class="cantidad form-group">
					{{ Form::label('cantidad', 'Stock') }}
					{{ Form::number('cantidad', $libro->cantidad, ['class' => 'form-control', 'placeholder' => 'Stock']) }}
				</div>
                <br>
				<div class="tapa form-group">
					{{ Form::label('tapa', 'Editar Imagen', ['class' => 'btn btn-primary']) }}<br>
					{{ Form::file('tapa', ['id' => 'tapa', 'style'=> 'display:none']) }}
					<div class="post-thumbnail" style="background-image: url({{resolveImage($libro->img)}}); width:70px; min-height: 105px; margin-top: 20px; background-size: contain"></div>
				</div><br><br>
				<div class="form-check">
					@php
					$selected = $libro->popup ? true : false;
					$selected1 = $libro->destacados1 ? true : false;
					$selected2 = $libro->destacados2 ? true : false;
					$selected3 = $libro->activado ? true : false;
					@endphp
            </div>
            <div id="switches">
            	<div class="d-flex justify-content-between">
            		<span>pop-up</span>
            		<label class="switch">
            			<input name="popup" type="checkbox" value="1" @if($selected) checked @endif>
            			<span class="slider round"></span>
            		</label>
            	</div>
            	<div class="d-flex justify-content-between mt-4">
            		<span>recomendados</span>
            		<label class="switch">
            			<input name="destacados1" type="checkbox" value="1" @if($selected1) checked @endif>
            			<span class="slider round"></span>
            		</label>
            	</div>
            	<div class="d-flex justify-content-between mt-4">
            		<span>novedades</span>
            		<label class="switch">
            			<input name="destacados2" type="checkbox" value="1" @if($selected2) checked @endif>
            			<span class="slider round"></span>
            		</label>
            	</div>
            	<div class="d-flex justify-content-between mt-4">
            		<span>activado</span>
            		<label class="switch">
            			<input name="activado" type="checkbox" value="1" @if($selected3) checked @endif>
            			<span class="slider round"></span>
            		</label>
            	</div>
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-12 px-5">
    		<div class="form-group">
    			{{Form::label('descripcion', 'Descripción')}}
    			{{Form::textarea('descripcion', $libro->descripcion, ['class' => 'form-control', 'placeholder' => 'descripción...', 'cols'=> 30, 'rows'=> '10'])}}
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-12 px-5">
    		{{Form::submit('Enviar', ['class' => 'btn btn-primary mt-2'])}}
    		<!-- fin . titulo form-group -->
    		{!! Form::close() !!}
    	</div>
    </div>
</div>
</section>
@endsection
