<?php

namespace Routes;

use App\Http\Controllers\CacheController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LibroController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use LaravelBA\RouteBinder\Routes;
use Illuminate\Contracts\Routing\Registrar;

class GeneralRoutes implements Routes {
    const CONTACTO = 'contacto';
    const HOME = 'home';
    const SHOP_BY_EDITOR = 'shop.by.editor';
    const SHOP_BY_AUTHOR = 'shop.by.author';

    public function addRoutes(Registrar $router)
    {
        $router->group(['middleware' => 'web'], function () use ($router) {

            $router->get('/', ['uses' => HomeController::class . '@index', 'as' => self::HOME]);
            $router->get('/libros/orden/editorial', LibroController::class . '@porEditorial');
            $router->get('/libros/orden/titulo', LibroController::class . '@porTitulo');

            $router->resources([
                'libros' => LibroController::class,
                'eventos' => PostController::class,
                'carrito' => CartController::class,
                'users' => UserController::class,
            ]);

            $router->get('/about', ['uses' => PageController::class . '@about']);
            $router->get('/shop/autor/{autor}', LibroController::class . '@byAuthor')->name(self::SHOP_BY_AUTHOR);
            $router->get('/shop/editorial/{editorial}', LibroController::class . '@byEditor')->name(self::SHOP_BY_EDITOR);
            $router->get('/shop', LibroController::class . '@productos');
            $router->get('/faq', ['uses' => PageController::class . '@faq']);
            $router->get('/ferias', ['uses' => PageController::class . '@ferias']);
            $router->get('/privacidad', ['uses' => PageController::class . '@privacidad']);
            $router->get('/terminos', ['uses' => PageController::class . '@terminos']);
            $router->get('/envios-y-devoluciones', ['uses' => PageController::class . '@envios']);

            $router->get('/search/admin', LibroController::class . '@buscarAdmin');
            $router->get('/search', LibroController::class . '@buscar');

            $router->post('/contacto', ['uses' => ContactController::class . '@contacto', 'as' => self::CONTACTO]);
        });
    }
}
