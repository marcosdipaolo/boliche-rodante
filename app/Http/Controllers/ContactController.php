<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Contacto;
use App\User;

class ContactController extends Controller
{
   	public function contacto(Request $request)
   	{
   		$user = new User;
   		$user->name = $request->nombre;
   		$user->email = $request->email;
   		$mensaje = $request->mensaje;
   		$admin_address =  \Config::get('app.email');
   		$email = new Contacto( $user, $mensaje );
   		\Mail::to($admin_address)->send($email);	
   		session()->flash('success', 'El mensaje se envió  con  éxito');
   		return redirect('/');	 	 
   	}
}
