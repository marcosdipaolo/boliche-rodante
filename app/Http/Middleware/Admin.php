<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()){
            if ( $request->user()->id != 1 ) {
                return redirect('/')->with('error', 'No estás autorizado para acceder a esa sección');
            } else {
                return $next($request);
            }    
        } else {
            session()->flash('No estás autorizado para acceder a esa sección');
            return redirect('/')->with('error', 'No estás autorizado para acceder a esa sección');
        }
        
        
    }
}
