@auth 
	
	@if(auth()->user()->id === 1)<div class="overlay"></div><a class="iconos-borrar-editar uno" href="/libros/{{$libro->slug}}/edit"><i class="far fa-edit"></i></a>
	<a href="" data-id="{{$libro->id}}" class="ml-4 sweet-alert-borrar iconos-borrar-editar dos">
		<i class="far fa-trash-alt"></i>
	</a>
    <form id="borrar_libro_{{$libro->id}}" action="{{route('libros.destroy', ['slug'=> $libro->slug])}}" method="POST" style="display: none;">
	    @method('DELETE')
	    @csrf
    </form>
	@endif 
@endauth