$(document).ready(function() {

    $('section#compras input.enviado').on('change', function(e) {

        var compra = $(this).data('compra');
        var $exito = $(this).parent().siblings('.alert-success');
        var $error = $(this).parent().siblings('.alert-danger');
        var data = {
            _token: $(this).siblings('input[name=_token]').val(),
            _method: $(this).siblings('input[name=_method]').val(),
            enviado: $(this).is(':checked') ? 1 : 0,
            compra: $(this).data('compra'),
        }

        $.ajax({
            type: 'POST',
            url: `/compras/${compra}`,
            data: data,
            success: compra => {
                if (compra.enviado === '1') $exito.text('El libro fue guardado como enviado');
                else $exito.text('El libro fue guardado como NO enviado');

                $exito.fadeIn(200);
                setTimeout(function() { $exito.fadeOut(200); }, 1000);
            },
            error: function() {
                $error.fadeIn(200);
                setTimeout(function() { $error.fadeOut(200); }, 1000);
            }

        });

    });



}); // final
