@if (count($errors))
    <div class="container alert alert-danger" style="width: 80%; margin: 30px auto">
        <ul style="text-align: center; list-style-type: none">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif