<?php

namespace App\Http\Controllers;

use App\Libro;
use App\Post;

class HomeController extends Controller
{
    public function index()
    {
        $eventos = Post::latest()->take(4)->get();
        return view('home', compact('eventos'));
    }
}
