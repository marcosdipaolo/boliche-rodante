<?php

if (!function_exists('paragraphize')) {
    function paragraphize(string $text)
    {
        $paragraph = str_replace("\r\n", "</p><p>", $text);
        return "<p>{$paragraph}</p>";
    }
}
if (!function_exists('resolveImage')) {
    function resolveImage(string $uri)
    {
       if ($uri && $uri !== '') {
           return \Storage::url($uri);
       }
       return asset('/images/default.jpg');
    }
}
