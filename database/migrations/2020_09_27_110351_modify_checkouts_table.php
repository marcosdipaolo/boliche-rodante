<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checkouts', function (Blueprint $table) {
            $table->dropColumn('order_id');
            $table->dropColumn('importe');
            $table->dropColumn('productos');
            $table->dropColumn('response');
            $table->dropColumn('data');
            $table->dropColumn('enviado');
            $table->text('cart');
            $table->boolean('sent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
