<?php

namespace App\Http\Controllers;

use App\Libro;
use App\PriceUpdate;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

class PriceUpdateController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('actualizar');
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'xls' => 'required|file|max:1000',
            'editorial' => 'string|required'
        ]);
        $update = new PriceUpdate;
        $update->editorial = $request->editorial;
        if ($request->hasFile('xls')) {
            $extension = $request->file('xls')->getClientOriginalExtension();
            $name = time();
            $path = $request->file('xls')->storeAs('public/xls', $name . '.' . $extension, 'local');
            $update->xls = $path;
        }
        $update->save();
        $this->leerXls($path, $extension);
        $libros = Libro::orderBy('titulo', 'ASC')->paginate(100);
        return redirect()->action(LibroController::class . '@index', ['libros' => $libros]);
    }

    /**
     * @param $path
     * @param $extension
     * @return bool
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function leerXls($path, $extension)
    {
        if ($extension == 'xls') {
            $reader = new Xls();
            $reader = IOFactory::createReader('Xls');
        } else {
            if ($extension == 'xlsx') {
                $reader = IOFactory::createReader('Xlsx');
            } else {
                return false;
            }
        }
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load(storage_path('app') . '/' . $path);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $allIsbns = [];
        $treceIsbns = [];
        $NoTreceIsbns = [];
        $NoEstanIsbns = [];
        $isbns = '';
        $fila = 1;
        foreach ($sheetData as $unPrecioArr) {
            $isbn = preg_replace('/\xA0/u', ' ', $unPrecioArr['A']);
            $isbn = trim(is_numeric($isbn) ? round($isbn) : $isbn);
            $allIsbns[] = $isbn;
            $precio = null;
            $cantidad = null;
            if (isset($unPrecioArr['B'])) {
                $precio = $unPrecioArr['B'] ? round($unPrecioArr['B']) : $unPrecioArr['B'];
            }
            if (isset($unPrecioArr['C'])) {
                $cantidad = $unPrecioArr['C'] ? round($unPrecioArr['C']) : $unPrecioArr['C'];
            }
            if (strlen($isbn) == 13) {
                $treceIsbns[] = $isbn;
                $libro = Libro::where('isbn', $isbn)->first();
                if ($libro) {
                    $libro->precio = $precio ?? $libro->precio;
                    $libro->cantidad = $cantidad ?? $libro->cantidad;
                    $libro->save();
                    request()->session()->flash('success', 'Se actualizaron los precios correctamente');
                } else {
                    $NoEstanIsbns[] = 'fila: ' . $fila . ': ' . $isbn;
                }
            } else {
                $NoTreceIsbns[] = $isbn;
            }
            $fila++;
        }
        if (count($NoEstanIsbns)) {
            request()->session()->flash('not_found',
                "No se encontraron " . count($NoEstanIsbns) . " libros. Los isbn son: \n" . implode(', ',
                    $NoEstanIsbns));
        }
        if (count($NoTreceIsbns)) {
            request()->session()->flash('error',
                'hubo ' . count($NoTreceIsbns) . ' error/es en la/s actualizacion/es. Ocurrió en el/los siguiente/s isbn/\'s: ' . implode(', ',
                    $NoTreceIsbns));
        }
    }
}
