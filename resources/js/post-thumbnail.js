// $('form#postPublish input#image').on('change', function() {
//     console.log(document.getElementById("image").files[0])
//     // $('form#postPublish div.post-thumbnail').css('background-image', document.getElementById("image").files[0].name);
// });
// 
// 
$(document).ready(function() {
    $("#tapa").change(function() {
        var length = this.files.length;
        if (!length) {
            return false;
        }
        changeBackground(this);
    });
});

// Creating the function
function changeBackground(img) {
    var file = img.files[0];
    var imagefile = file.type;
    var match = ["image/jpeg", "image/png", "image/jpg"];
    if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
        alert("Invalid File Extension");
    } else {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(img.files[0]);
    }

    function imageIsLoaded(e) {
        $('div.post-thumbnail').css({ 'background-image': "url(" + e.target.result + ")" });

    }
}
