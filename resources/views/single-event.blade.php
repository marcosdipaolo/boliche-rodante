@extends('layouts.master')

@section('content')
<span id="body_class" data-class="single-event" class="d-none"></span>

@include('layouts.single-event')

@endsection