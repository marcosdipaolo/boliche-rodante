@extends('editar.app')

@section('content')
@php
    $pagina = isset($_GET['page']) ? "?page={$_GET['page']}" : '';
@endphp
<section id="editar-libros">
<span id="body_class" data-class="admin" class="d-none"></span>
    @include('editar.botones')
    <br>
<div class="container">
	<br>
	<form id="search-admin" action="/search/admin" method="GET">
		@csrf
		<input class="d-block w-100" type="text" name="query"><i onclick="document.querySelector('form#search-admin').submit()" class="fas fa-search" style="cursor: pointer"></i>
	</form>
	<br>
    {{$libros->links()}}
    @isset($cadena)
        <h2 class="text-center my-5">Busqueda de libros: {{$cadena}}</h2>
    @else
        <h2 class="text-center my-5">Lista de libros</h2>
        @isset($errors)
            @include('layouts.errors')
        @endisset
    @endisset
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
            </div>
        @endif
        @if (session('not_found'))
            <div class="alert alert-danger" role="alert">
                {{ session('not_found') }}
            </div>
        @endif
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <style>
                tr th, tr td {
                    text-align: center;
                }
            </style>
            @if (session('exito'))
                <div class="alert alert-success" role="alert">
                    {{ session('exito') }}
                </div>
            @endif
            <table class="table libros">
            <tr>
                <th>id</th>
                <th>isbn</th>
                <th><a class="orden" href="/libros/orden/titulo{{$pagina}}">título <i class="fas fa-caret-down"></i></th>
                <th><a class="orden" href="/libros/orden/editorial{{$pagina}}">editorial <i class="fas fa-caret-down"></i></a></th>
                <th>imagen</th>
                <th>editar</th>
            </tr>
             @foreach($libros as $libro)
            <tr class="">
                <td>{{$libro->id}}</td>
                <td>{{$libro->isbn}}</td>
                <td>{{$libro->titulo}}</td>
                <td>{{$libro->editorial}}</td>
                <td><img src="{{resolveImage($libro->img)}}" width="50" alt=""></td>
                <td><a href="/libros/{{$libro->slug}}/edit"><i class="far fa-edit"></i></a>
                    <a data-id="{{$libro->id}}" href="" class="ml-2 sweet-alert-borrar"><i class="far fa-trash-alt"></i></a></td>
                <td style="display: none;"><form id="borrar_libro_{{$libro->id}}" action="{{route('libros.destroy', ['slug'=> $libro->slug])}}" method="POST" style="display: none;">
                    <!-- UNO -->
                    @method('DELETE')
                    <!-- DOS -->
                    @csrf
                    <!-- TRES -->
                </form></td>
            </tr>
            @endforeach
        </table>
        {{$libros->links()}}
        </div>
    </div>
</section>
@endsection
