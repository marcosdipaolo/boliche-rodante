<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Edad
 *
 * @property int $id
 * @property string $edad
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Libro[] $libros
 * @property-read int|null $libros_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Edad newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Edad newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Edad query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Edad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Edad whereEdad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Edad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Edad whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Edad extends Model
{
    protected $table = 'edades';

    /**
     * @return BelongsToMany
     */
    public function libros()
    {
    	return $this->belongsToMany(Libro::class);
    }
}
