<?php 
namespace App\Traits;
use App\User;

trait UserHasNullFields
{
	public function hasNullFields() 
	{
		if ( ! $this->nombre || ! $this->apellido || ! $this->direccion || ! $this->ciudad || ! $this->provincia || ! $this->pais || ! $this->cp || ! $this->telefono ) {
			return true;
		} else {
			return false;
		}
	}
}