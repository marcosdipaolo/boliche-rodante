<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\MPPreference
 *
 * @property int $id
 * @property string|null $preference_id
 * @property string|null $external_reference
 * @property string|null $back_url
 * @property string|null $payment_id
 * @property string|null $payment_status
 * @property string|null $payment_status_detail
 * @property string|null $merchant_order_id
 * @property string|null $processing_mode
 * @property string|null $merchant_account_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference whereBackUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference whereExternalReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference whereMerchantAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference whereMerchantOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference wherePaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference wherePaymentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference wherePaymentStatusDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference wherePreferenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference whereProcessingMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MPPreference whereUpdatedAt($value)
 * @mixin Eloquent
 */
class MPPreference extends Model
{
    protected $guarded = [];
}
