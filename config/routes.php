<?php
use Symfony\Component\Finder\Finder;

$binders = [];

foreach (Finder::create()->files()->in(base_path('routes')) as $file) {
    $binders[] = "Routes\\" . str_replace('.php', '', $file->getFilename());
}

return ['binders' => $binders];
