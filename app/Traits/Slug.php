<?php 
namespace App\Traits;
use App\Libro;
use App\Post;
trait Slug 
{
    public function slug() 
    {
        $id = $this->id ?? 0;
        $titulo = $this->titulo;
        $titulo = iconv('UTF-8', 'ASCII//TRANSLIT', $titulo);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $titulo);
        $slug = strtolower(trim($slug, '-'));
        $slug = preg_replace("/[\/_|+ -]+/", "-", $slug);
        while (substr($slug, -1, 1) == "-") {
            $slug = substr($slug, 0, -1);
        }
        while (substr($slug, 0, 1) == "-") {
            $slug = substr($slug, 1, 100);
        }
        if ($this instanceof Libro) {
            if (Libro::where('slug', $slug)->exists() && Libro::where('slug', $slug)->first()->id != $id ) {
                return $slug . '-' . $id;
            } else {
                return $slug;            
            }
        }
        if ($this instanceof Post) {
            if (Post::where('slug', $slug)->exists() && Post::where('slug', $slug)->first()->id != $id ) {
                return $slug . '-' . $id;
            } else {
                return $slug;            
            }
        }
    }
}
