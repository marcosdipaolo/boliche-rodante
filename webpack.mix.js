const mix = require('laravel-mix');
mix.options({
    processCssUrls: false
});
mix.sourceMaps();
mix.webpackConfig({
    devtool: "inline-source-map"
});
mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/post-thumbnail.js', 'public/js')
    .js('resources/js/precios.js', 'public/js')
    .js('resources/js/compras.js', 'public/js')
    .js('resources/js/filters.js', 'public/js')
    .sass('resources/sass/styles.scss', 'public/css');
