<section class="about-sec parcial">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 px-0">
                <div class="about-img">
                    <figure style="background:url(/images/about-img.jpg)no-repeat;"></figure>
                </div>
            </div>
            <div class="col-md-7 px-0">
                <div class="about-content">
                    <h2>Nosotros...</h2>
                    <p>Somos feriantes de Literatura Infantil. Somos inquietos y nos gusta pensar que llevamos con nosotros universos que despiertan la imaginación en chicos y grandes. Hacemos delivery por algunos barrios de la ciudad acercando los pedidos a tu casa y también tenemos un espacio en Belgrano que podés visitar con cita previa.</p><p>Podemos sugerirte el mejor regalo para cada ocasión. Nos contás tus inquietudes, temas o gustos y nos acercamos llenos de libros para que puedas elegir. Tenemos toda la colección de La Brujita de Papel, Pequeño Editor, Ojoreja, Chirimbote, Calibroscopio, Del Naranjo, Del Eclipse, Fondo de Cultura Económica, Ekaré, Colihue, Iamiqué, Kalandraka, UnaLuna entre otras...</p>
                    <div class="btn-sec">
                        <a href="/shop" class="btn yellow">comprar libros</a>
                        @guest
                        <a href="/register" class="btn black">registrate</a>
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</section>