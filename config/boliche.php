<?php
return [
    'direccion' => env('DIRECCION', 'Solicitar Dirección por privado'),
    'whatsapp' => env('WHATSAPP', '#1167634382'),
];
