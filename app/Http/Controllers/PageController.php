<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function about()
    {
        return view('about');
    }

    public function faq()
    {
        return view('faq-page');
    }

    public function ferias()
    {
        return view('ferias');
    }

    public function privacidad()
    {
        return view('privacidad');
    }

    public function terminos()
    {
        return view('terminos');
    }

    public function envios()
    {
        return view('envios-y-devoluciones');
    }
}
