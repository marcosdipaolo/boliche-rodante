<!-- edit-user -->
<section class="pagina" id="edit-user">
	<div class="container">
		@include('layouts.errors')
	    <h4>Editar perfil de usuario</h4><br><br>
	    {!! Form::open(['action' => [ App\Http\Controllers\UserController::class .  '@update', auth()->user()->id], 'method' => 'PUT']) !!}
			<div class="row">
				<div class="col-sm-6 px-4">
					{{ Form::label('', 'Nombre de usuario') }}
					{{ Form::text('', '@' . $user->name, ['disabled']) }}
			    	{{ Form::label('nombre', 'Nombre') }}
			    	{{ Form::text('nombre' , $user->nombre) }}
			    	{{ Form::label('apellido', 'Apellido') }}
			    	{{ Form::text('apellido', $user->apellido) }}
			    	{{ Form::label('email', 'E-mail') }}
			    	{{ Form::text('email', $user->email, ['disabled']) }}
			    	{{ Form::label('direccion', 'Dirección') }}
			    	{{ Form::text('direccion', $user->direccion) }}
			    </div>
			    <div class="col-sm-6 px-4">
			    	{{ Form::label('cp', 'C.P.') }}
					{{ Form::text('cp', $user->cp) }}
			    	{{ Form::label('ciudad', 'Ciudad') }}
			    	{{ Form::text('ciudad' , $user->ciudad) }}
			    	{{ Form::label('provincia', 'Provincia') }}
			    	{{ Form::select('provincia', $provincias, $user->provincia,['class' => 'form-control my-3', 'style' => 'position: relative; top: -12px' , 'placeholder' => 'Elegir Provincia']) }}
			    	{{ Form::label('pais', 'País') }}
			    	{{ Form::text('pais', 'Argentina', ['disabled']) }}
			    	{{ Form::label('telefono', 'Teléfono') }}
			    	{{ Form::text('telefono', $user->telefono) }}
			    </div>
	    	</div>
	    	<span style="color:red">*</span>&nbsp;&nbsp;<small>Si deseás cambiar tu nombre de usuario o email ponete en contacto con nosotros</small><br><br>
	    	<p class="text-center my-0"><button class="btn btn-orange">Enviar</button></p>
	    </form>
        <br>

	</div>
</section>
<!-- fin . edit-user -->
