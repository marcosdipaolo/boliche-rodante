@php
$carrito = session('carrito');
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Boliche Rodante</title>
	@include('layouts.header-inc.metas')
	@include('layouts.header-inc.links')
</head>

<body>
	<header id="header" class="clearfix">
		@include('layouts.header-inc.header-top')
		@include('layouts.header-inc.main-menu')
		
	</div>
</div>
</header>

<div class="placeholder"></div>
</div>
