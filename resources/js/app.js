$(document).ready(function() {
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function() {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });

    /* clases del body */

    $('body').addClass($('span#body_class').data('class'));


    /**
     * boton TODOS LOS LIBROS
     */
    if ($('body').hasClass('home')) {
        $('div.btn-sec a.gray-btn').attr('href', '/shop');
        $('div.btn-sec a.gray-btn').text('Mirá mas libros');
    } else {
        $('div.btn-sec a.gray-btn').on('click', e => {
            e.preventDefault();
            $('section.all-books').slideToggle();
        });
    }

    /*quill*/
    $('#submit-post').on('click', e => {
        let content = $('div.ql-editor').html();
        e.preventDefault();
        $('input#texto').val(content);
        $('#edit-post').submit();

    });

    /*********************************/

    $('span#lupa').on('click', () => {
        $('form#search').submit();
    });

    ///// scrollReveal js //////////
    window.sr = ScrollReveal();
    sr.reveal('.parcial', { duration: 500, origin: 'left', scale: 0.8, easing: 'ease-in-out' });

    /**
     * sweet alert
     */


    $('a.sweet-alert-borrar').on('click', function(e) {
        e.preventDefault();
        let id = $(this).data('id');
        var response = false;
        swal({
            title: "Estás seguro ? ",
            text: "Esta acción no tiene vuelta atrás",
            type: "question",
            showCancelButton: true,
            showConfirmButton: true,
            confirmButtonColor: '#F0563A',
            confirmButtonText: 'Si, estoy seguro!',
            cancelButtonText: "No, cancelar!",
            showLoaderOnConfirm: true,
            preConfirm: function() {
                response = true;
            }
        }).then(function() {
            if (response) {
                $('form#borrar_libro_' + id).submit();
            } else return;
        });
    });
});
