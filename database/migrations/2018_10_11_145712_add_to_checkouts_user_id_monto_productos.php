<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToCheckoutsUserIdMontoProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checkouts', function (Blueprint $table) {
                $table->integer('user_id')->after('order_id');
                $table->float('importe', 8, 2)->after('user_id');
                $table->string('productos', 500)->after('importe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkouts', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('importe');
            $table->dropColumn('productos');
        });
    }
}
