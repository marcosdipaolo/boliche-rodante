<?php

namespace App\Http\Controllers;

use App\Libro;
use App\Traits\PaginateItems;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class LibroController extends Controller
{
    use PaginateItems;

    public function __construct()
    {
        $this->middleware(['auth', 'admin'])->except('show', 'buscar', 'productos', 'byAuthor', 'byEditor');
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $libros = Libro::orderBy('created_at', 'DESC')->paginate(20);
        return view('editar.libros', compact('libros', 'recientes', 'recomendados'));
    }

    /**
     * @return View
     */
    public function productos(): View
    {
        $edades = \App\Edad::all();
        $generos = \App\Genero::all();
        $infos = \App\Info::all();
        return view('productos', compact('edades', 'generos', 'infos'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function buscar(Request $request)
    {
        // return dd($request->page);
        $request->validate([
            'query' => 'required|min:2|string'
        ]);
        $cadena = $request->input('query');
        $libros = Libro::search($cadena)->get()->where('activado', 1)->where('img', '<>', '')   ;
        $queryString = '/search?query=' . $cadena;
        $librosPaged = $this->getPaginatedItems($libros, 12, $queryString);

        return view('search.libros', compact('librosPaged', 'cadena'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function buscarAdmin(Request $request)
    {
        $request->validate([
            'query' => 'bail|required|min:2|string'
        ]);
        $cadena = $request->input('query');
        $librosCollection = Libro::search($cadena)->get();
        $queryString = '/search?query=' . $cadena;
        $libros = $this->getPaginatedItems($librosCollection, 10, $queryString);

        return view('editar.libros', compact('libros', 'cadena'));
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $edades = \App\Edad::all();
        $edadesNombres = [];
        $generosNombres = [];
        $infosNombres = [];
        foreach ($edades as $edad) {
            $edadesNombres[$edad->id] = $edad->edad;
        }
        $generos = \App\Genero::all();
        foreach ($generos as $genero) {
            $generosNombres[$genero->id] = $genero->genero;
        }
        $infos = \App\Info::all();
        foreach ($infos as $info) {
            $infosNombres[$info->id] = $info->info;
        }
        return view('editar.crear-libro', compact('edadesNombres', 'generosNombres', 'infosNombres'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tapa' => 'image|max:1000',
            'titulo' => 'string|required',
            'autor' => 'string|required',
            'isbn' => 'string|required',
            'editorial' => 'string|required',
        ]);
        $popup = $request->popup ?? 0;
        $destacados1 = $request->destacados1 ?? 0;
        $destacados2 = $request->destacados2 ?? 0;
        $activado = $request->activado ?? 0;
        $libro = new Libro;
        $libro->titulo = $request->titulo;
        $libro->slug = $libro->slug();
        $libro->isbn = $request->isbn;
        if ($request->hasFile('tapa')) {
            $img_path = $libro->subirImagen($request->tapa);
        } else {
            $img_path = $libro->img ?? '/images/default.jpg';
        }
        $libro->editorial = $request->editorial;
        $libro->autor = $request->autor;
        $libro->descripcion = $request->descripcion ?? '';
        $libro->coleccion = $request->coleccion;
        $libro->distribuidor = $request->distribuidor;
        $libro->precio = $request->precio;
        $libro->cantidad = $request->cantidad;
        $libro->popup = $popup;
        $libro->destacados1 = $destacados1;
        $libro->destacados2 = $destacados2;
        $libro->activado = $activado;
        $libro->anio = $request->anio;
        $libro->img = $img_path;
        $libro->save();
        if ($request->info) {
            foreach ($request->info as $info) {
                $libro->infos()->attach($info);
            }
        }
        if ($request->genero) {
            foreach ($request->genero as $genero) {
                $libro->generos()->attach($genero);
            }
        }
        if ($request->edad) {
            foreach ($request->edad as $edad) {
                $libro->edades()->attach($edad);
            }
        }
        session()->flash('exito', 'El libro fue creado editado con éxito');
        return redirect('/libros');
    }

    /**
     * @param Libro $libro
     * @return Factory|View
     */
    public function show(Libro $libro)
    {
        return view('show-libro', compact('libro'));
    }

    /**
     * @param Libro $libro
     * @return Factory|View
     */
    public function edit(Libro $libro)
    {
        $edades = \App\Edad::all();
        $generos = \App\Genero::all();
        $infos = \App\Info::all();
        return view('editar.editar', compact('edades', 'infos', 'generos', 'libro'));
    }

    /**
     * @param Request $request
     * @param Libro $libro
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function update(Request $request, Libro $libro)
    {

        $this->validate($request, [
            'tapa' => 'image',
            'titulo' => 'string|required',
            'autor' => 'string|required',
            'isbn' => 'string|required',
            'editorial' => 'string|required'
        ]);
        $popup = $request->popup ?? 0;
        $destacados1 = $request->destacados1 ?? 0;
        $destacados2 = $request->destacados2 ?? 0;
        $activado = $request->activado ?? 0;
        $libro->titulo = $request->titulo;
        $libro->slug = $libro->slug();
        if ($request->hasFile('tapa')) {
            $img_path = $libro->subirImagen($request->tapa);
        } else {
            $img_path = $libro->img ?? '/images/default.jpg';
        }
        $libro->editorial = $request->editorial;
        $libro->autor = $request->autor;
        $libro->coleccion = $request->coleccion;
        $libro->isbn = $request->isbn;
        $libro->distribuidor = $request->distribuidor;
        $libro->descripcion = $request->descripcion ?? '';
        $libro->precio = $request->precio;
        $libro->cantidad = $request->cantidad;
        $libro->popup = $popup;
        $libro->destacados1 = $destacados1;
        $libro->destacados2 = $destacados2;
        $libro->activado = $activado;
        $libro->anio = $request->anio;
        $libro->img = $img_path;
        $libro->save();
        $libro->edades()->detach();
        $libro->generos()->detach();
        $libro->infos()->detach();
        if ($request->info) {
            foreach ($request->info as $info) {
                $libro->infos()->attach($info);
            }
        }
        if ($request->genero) {
            foreach ($request->genero as $genero) {
                $libro->generos()->attach($genero);
            }
        }
        if ($request->edad) {
            foreach ($request->edad as $edad) {
                $libro->edades()->attach($edad);
            }
        }
        session()->flash('exito', 'El libro fue editado con éxito');
        return redirect('/libros');
    }

    /**
     * @param Request $request
     * @param Libro $libro
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(Request $request, Libro $libro)
    {

        $libro->delete();
        $libro->borrarImg();
        session()->flash('exito', 'El libro se borró con éxito');
        return redirect('/libros');
    }

    /**
     * @return Factory|View
     */
    public function indexPrecios()
    {
        $libros = Libro::orderBy('titulo', 'ASC')->paginate(100);
        return view('precios', compact('libros'));
    }

    /**
     * @return Factory|View
     */
    public function porEditorial()
    {
        $libros = Libro::orderBy('editorial', 'ASC')->paginate(20);
        return view('editar.libros', compact('libros'));
    }

    /**
     * @return Factory|View
     */
    public function porTitulo()
    {
        $libros = Libro::orderBy('titulo', 'ASC')->paginate(20);
        return view('editar.libros', compact('libros'));
    }

    public function byAuthor($autor)
    {
        $libros = Libro::where('autor', $autor)->where('img', '<>', '')->paginate(18);
        return view('archive', compact('libros', 'autor'));
    }

    public function byEditor($editorial)
    {
        $libros = Libro::where('editorial', $editorial)->where('img', '<>', '')->paginate(18);
        return view('archive', compact('libros', 'editorial'));
    }
}
