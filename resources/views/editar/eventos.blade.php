@extends('editar.app')

@section('content')
<section id="editar-eventos">
	<span id="body_class" data-class="admin" class="d-none"></span>
	<div class="container">
		@if(session()->has('exito')) 
		<div class="alert success-alert">{{session('exito')}}</div>
		@endif
	</div>
	    @include('editar.botones')
        @php
            $eventos = App\Post::orderBy('created_at', 'DESC')->paginate(20);
        @endphp
        {{$eventos->links()}}
    <div class="container">
	    <h3 class="mb-4">Lista de eventos</h3>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <style>
                tr th, tr td {
                    text-align: center;
                }
            </style>
            @if (session('exito'))
                <div class="alert alert-success" role="alert">
                    {{ session('exito') }}
                </div>
            @endif
            <table class="table libros">
	            <tr>
	                <th>id</th>
	                <th>título</th>
	                <th>imagen</th>
	                <th>editar</th>
	            </tr>
	             @foreach($eventos as $evento)
	            <tr class="">
	                <td>{{$evento->id}}</td>
	                <td><a href="/eventos/{{$evento->slug}}">{{$evento->titulo}}</a></td>
	                <td><img src="{{Config::get('app.url')}}{{Storage::url($evento->img)}}" width="100"></td>
	                <td><a href="/eventos/{{$evento->slug}}/edit"><i class="far fa-edit"></i></a><a href="" class="ml-4" onclick="event.preventDefault(); if(confirm('Estás segure?')){document.getElementById('borrar_libro_{{$evento->id}}').submit();} else {return;}"><i class="far fa-trash-alt"></i></a></td>
	                <td style="display: none;"><form id="borrar_libro_{{$evento->id}}" action="{{route('eventos.destroy', ['slug'=> $evento->slug])}}" method="POST" style="display: none;">
	                    <!-- UNO -->
	                    @method('DELETE')
	                    <!-- DOS -->
	                    @csrf
	                    <!-- TRES -->
	                </form></td>
	            </tr>
	            @endforeach   
        	</table>
        {{$eventos->links()}}
        </div>
	</div>
</section>
@endsection