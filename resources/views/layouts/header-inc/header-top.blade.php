<div class="header-top">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-6">
				<a href="{{Config::get('app.url')}}" class="web-url">www.bolicherodante.com
				</a>
			</div>
			<div class="col-sm-6">
				<!-- Right Side Of Navbar -->
				<ul class="navbar-nav ml-auto py-0">
					<!-- Authentication Links -->
					@auth @if( auth()->user()->id === 1 )<li class="nav-item pr-4"><a class="nav-link" href="/libros">admin</a></li> @endif @endauth
					@guest
					<li class="nav-item pr-4">
						<a class="nav-link" href="{{ route('login') }}">{{ __('Iniciá sesión') }}</a>
					</li>
					<li class="nav-item">
						@if (Route::has('register'))
						<a class="nav-link" href="{{ route('register') }}">{{ __('Registrate') }}</a>
						@endif
					</li>
					@else
					<li class="nav-item dropdown">
						<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
							{{ Auth::user()->name }} <span class="caret"></span>
						</a>

						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="/users/{{auth()->user()->id}}">
								{{ __('Perfil') }}
							</a>
							<a class="dropdown-item" href="{{ route('logout') }}"
							onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">
							{{ __('Logout') }}
						</a>


						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</li>
					@endguest
				</ul>
			</div> <!-- /.col-sm-6 --> 
		</div> <!-- /.row -->
	</div> <!-- /.container -->
</div> <!-- /.header-top -->