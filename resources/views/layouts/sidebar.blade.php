	<aside>
		@if(count($recomendados))
		<h5>Recomendados</h5>
		<hr>
		@foreach ($recomendados as $recom)
			<!-- . widget -->
			<div class="widget clearfix">
			    <img src="{{Storage::url($recom->img)}}" width="50">
			    <p><a href="/libros/{{$recom->slug}}">{{$recom->titulo}}</a></p>
			    <small>{{$recom->autor}}</small><br>
			    <small class="text-danger font-weight-bold">${{$recom->precio}}.-</small>
                @if(!$recom->cantidad <= 0)
			    <form method="POST" action="/carrito" class="d-inline ml-2">
			    	@csrf
			    	<input type="hidden" name="id" value="{{$recom->id}}">
			    	<button class="no-style"><i class="fas fa-cart-plus"></i></button>
			    </form>
                @endif
                @if($recom->cantidad <= 0)
                <div class="position-relative">
                    <span class="badge badge-danger">No hay stock</span>
                </div>
                @endif
			</div>
			<hr>
			<!-- fin . widget -->
		@endforeach
		<br><br>
		@endif
		@if(count($recientes))
		<h5>Últimas Novedades</h5>
		<hr>
		@foreach ($recientes as $recent)
			<!-- . widget -->
			<div class="widget clearfix">
			    <img src="{{Storage::url($recent->img)}}" width="50">
			    <p><a href="/libros/{{$recent->slug}}">{{$recent->titulo}}</a></p>
			    <small>{{$recent->autor}}</small><br>
			    <small class="text-danger font-weight-bold">${{$recent->precio}}.-</small>
                @if(!$recent->cantidad <= 0)
			    <form method="POST" action="/carrito" class="d-inline ml-2">
			    	@csrf
			    	<input type="hidden" name="id" value="{{$recent->id}}">
			    	<button class="no-style"><i class="fas fa-cart-plus"></i></button>
			    </form>
                @endif
                @if($recent->cantidad <= 0)
                    <div class="position-relative">
                        <span class="badge badge-danger">No hay stock</span>
                    </div>
                @endif
			</div>
			<hr>
			<!-- fin . widget -->
		@endforeach
		<br><br>
		@endif
		@if(count($sidebar_posts))
		<div class="eventos">
			<h5>Eventos</h5>
			<hr>
			@foreach ($sidebar_posts as $post)
				<!-- . widget -->
				<div class="widget clearfix">
				    <img src="{{Storage::url($post->img)}}" width="80">
				    <p><a href="/eventos/{{$post->slug}}">{{$post->titulo}}</a></p>
					<small style="color: grey; font-size:11px;" >{{str_limit(strip_tags($post->texto), 25)}}</small>
				</div>
				<hr>
				<!-- fin . widget -->
			@endforeach
		</div>
		@endif
	</aside>
