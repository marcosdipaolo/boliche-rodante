<section class="static about-sec pagina">
    <style>
        #terminos ul {
            list-style-type: disc;
            list-style-position: inside;
        }
        #terminos ul li {
            font-weight: 400
        }
    </style>
	<div id="terminos" class="container">
		<h2>Términos y Condiciones</h2><br><br>
		<h3>Selección de productos</h3>
        <ul>
            <li>Seleccioná el producto que querés comprar y hacé click en el <strong>titulo</strong> + <strong>agregar al carrito.</strong></li>
            <li>Aparecerá una ventana indicando los Items agregados al carrito de compras. Hacé click en Bolsa de compras.</li>
            <li>Podés elegir 2 opciones, Seguir Comprando o checkout</li>
            <li>Para elegir mas de una unidad del mismo producto deberás volver repetir el primer paso.</li>
            <li>Al elegir Checkout, podrás ver los Items agregados al carrito de compras.</li>
        </ul>
        <br>

		<h3>Ingreso de datos</h3>
        <ul>
            <li>Si aún no iniciaste sesión en el sitio, ingresá tu email y clave en este paso.</li>
            <li>Si no tenés una cuenta en bolicherodante.com, haz click <a href="{{config('app.url')}}/register">aquí</a>.</li>
            <li>Luego de registrarte, para realizar la compra, deberás completar tus datos obligatorios</li>
        </ul>
        <br>
        <h3>Medio de pago</h3>
        <ul>
            <li>Al seleccionar PAGAR, podrás Elegir el medio de pago para realizar la compra.</li>
            <li>Si desea realizar  Transferencia electrónica, deberá solicitar nuestros datos.</li>
        </ul>
        <br>
        <h3>¿Cómo se realizan los envíos?</h3>
        <p>Para los envíos dentro de Argentina utilizamos Correo Argentino, Oca o encomienda, en tanto que para los envíos al exterior, nos manejamos solo con la empresa DHL.</p>
        <br>
        <h3>¿Cuál es el costo de envío dentro de Argentina?</h3>
        <p>El costo de envío se acordara vía correo electrónico o al Whatsapp: {{config('boliche.whatsapp')}}.<br>Puede estar bonificado dependiendo del lugar de entrega y/ o el costo sera el que cobre la empresa de envío que se convenga.
        </p>
        <br>
        <h3>¿Cuáles son los tiempos de entrega estimados?</h3>
        <p>Es de 3 a 7 días hábiles el tiempo que demoran las empresas, a partir de la confirmación del pago.</p>
        <br>
        <h3>¿Cuál es el plazo para realizar un cambio? </h3>
        <p>El plazo es de 15 días corridos después de realizada la compra.</p>


        <br>
		</div>
</section>
