<section class="pagina" id="carrito">
	<div class="container">
		@if(session()->has('checkoutSuccess'))
		<div class="alert alert-success">{{session()->get('checkoutSuccess')}}</div>
		@elseif(session()->has('checkoutError'))
		<div class="alert alert-danger">{{session()->get('checkoutError')}}</div>
		@endif
        @include('layouts.errors')
		@php
	    	$cart = Session::has('cart') ? Session::get('cart') : NULL;
	    	$cart_items = $cart ? ($cart->items ?? []) : [];
	    	$totalPrice = $cart->totalPrice ?? 0;
	    @endphp
	    <div class="row">
	    	<div class="col-md-8 cart-table mb-5">
	    		<h5>Items agregados al carrito de compras.</h5>
				<table class="table">
				  <thead>
				    <tr>
				      <th scope="col" class="d-none d-sm-block">#</th>
				      <th scope="col">Tapa</th>
				      <th scope="col">Título</th>
				      <th scope="col">Cantidad</th>
				      <th scope="col">Precio</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach ($cart_items as $item)
				  	@php
				  		$libro = $item['item'];
				  	@endphp
					    <tr>
					      <th scope="row" class="d-none d-sm-block">{{$loop->iteration}}</th>
					      <td><img src="{{resolveImage($libro->img)}}" width="50"></td>
					      <td><a href="/libros/{{$libro->slug}}">{{$libro->titulo}}</a><br><small>{{$libro->autor}}.</small><br><small>Editorial {{$libro->editorial}}</small></td>
					      <td align="center">{{$item['cant']}}
							@if($item['cant'] > 0)
						      	<br>
						      	<br>
						      	<form action="/carrito/{{$libro->id}}" method="POST">
						      		@method('PUT')
						      		@csrf
						      		<button class="no-style"><i class="far fa-trash-alt"></i></button>
						      	</form>
					      	@endif
					      </td>
					      <td>${{$item['precio']}}.-</td>
					    </tr>
			    	@endforeach
				  </tbody>
				  <tfoot>
				  		<th scope="col" class="d-none d-sm-block"></th>
				      	<th scope="col"></th>
				     	<th scope="col">TOTAL</th>
				     	<th scope="col"></th>
				      	{{-- <th scope="col">${{Cart::getSubTotal()}}</th> --}}
				      	<th scope="col" class="text-danger">${{$cart->totalPrice ?? 0}}.-</th>
				  </tfoot>
				</table><br>
				<a href="/shop"><button class="btn orange">SEGUIR comprando</button></a>
				@if( $totalPrice > 0 ) <a href="/checkout"><button class="btn black">Checkout</button></a>@endif
			</div>
			<!-- SIDEBAR -->
			<div id="aside-cont" class="col-md-4">
				@include('layouts.sidebar')
			</div>
			<!-- FIN SIDEBAR -->
		</div>

    </div>
</section>
<!-- fin . carrito -->
