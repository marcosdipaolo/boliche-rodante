@extends('layouts.master')
@section('content')
    @include('layouts.archive-books')
    <div class="container text-center mb-5">
        <a class="btn" href="{{url()->previous()}}">Volver</a>
    </div>
@endsection
