<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Genero
 *
 * @property int $id
 * @property string $genero
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Libro[] $libros
 * @property-read int|null $libros_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genero newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genero newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genero query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genero whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genero whereGenero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genero whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genero whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Genero extends Model
{

    /**
     * @return BelongsToMany
     */
    public function libros()
    {
    	return $this->belongsToMany(Libro::class);
    }
}
