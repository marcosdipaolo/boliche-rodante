@extends('layouts.master')

@section('content')
<span id="body_class" data-class="checkout" class="d-none"></span>

@include('layouts.checkout')

@endsection